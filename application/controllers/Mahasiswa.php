<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mahasiswa extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 *        http://example.com/index.php/welcome
	 *    - or -
	 *        http://example.com/index.php/welcome/index
	 *    - or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('form_validation', 'template'));
		$this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
	}

	public function index()
	{
		$data["page"] = "mahasiswa/main_menu";
		$this->load->view("mahasiswa/index", $data);
	}

	public function inputIdeJudul()
	{
		$data["page"] = "mahasiswa/input_ide_judul";
		$this->load->view("mahasiswa/index", $data);
	}


	//=================================================================================================================//
	//upload
	public function upload(){
        $fileName = time().$_FILES['file']['name'];
         
        $config['upload_path'] = './data/'; //buat folder dengan nama assets di root folder
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'xls|xlsx|csv';
        $config['max_size'] = 10000;
         
        $this->load->library('upload');
        $this->upload->initialize($config);
         
        if(! $this->upload->do_upload('file') )
        $this->upload->display_errors();
             
        $media = $this->upload->data('file');
        $inputFileName = './data/'.$media['file_name'];
         
        try {
                $inputFileType = IOFactory::identify($inputFileName);
                $objReader = IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
            } catch(Exception $e) {
                die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
            }
 
            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();
             
            for ($row = 2; $row <= $highestRow; $row++){                  //  Read a row of data into an array                 
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                NULL,
                                                TRUE,
                                                FALSE);
                                                 
                //Sesuaikan sama nama kolom tabel di database                                
                 $data = array(
                    "id_mahasiswa"=> $rowData[0][0],
                    "NPM"=> $rowData[0][1],
                    "nama_mahasiswa"=> $rowData[0][2],
					"judul"=> $rowData[0][3],
					"ipk" => $rowData[0][4],
                );
                 
                //sesuaikan nama dengan nama tabel
                $insert = $this->db->insert("eimport",$data);
                delete_files($media['file_path']);
                     
            }
        redirect('admin/data_mahasiswa');
    }
	//=================================================================================================================//
	public function index_beranda()
	{
		$data['page'] = 'user/mahasiswa/view_mahasiwa';
		$this->load->view('index', $data);
	}

	public function create()
	{
		if (substr(trim(strtoupper($this->input->post('NPM'))), 0, 3) === "G1A") {
			$data_cart = array(
				'NPM' => trim(strtoupper($this->input->post('NPM'))),
				'nama_mahasiswa' => trim($this->input->post('nama_mahasiswa')),
				'tanggal_masuk' => trim($this->input->post('tanggal_masuk'))
			);
			$this->model_mahasiswa->create($data_cart);
			redirect('admin/data_mahasiswa');
		} else {
			$this->session->set_flashdata("error", "NPM harus dimulai dari G1A");
			redirect('admin/input_mahasiswa');
		}
	}

	public function create_toefl()
	{
		$NPM = trim(strtoupper($this->input->post('NPM')));
		$data_cart = array(
			'IPK' => trim($this->input->post('IPK')),
			'TOEFL' => trim($this->input->post('TOEFL')),
			'TOEFL_2' => trim($this->input->post('TOEFL_2')),
			'TOEFL_3' => trim($this->input->post('TOEFL_3')),
			'tanggal_TOEFL_dibuat' => trim($this->input->post("tanggal_TOEFL_dibuat")),
			'tanggal_TOEFL_2_dibuat' => trim($this->input->post("tanggal_TOEFL_2_dibuat")),
			'tanggal_TOEFL_3_dibuat' => trim($this->input->post("tanggal_TOEFL_3_dibuat"))
		);
		$this->model_mahasiswa->update_toefl($NPM, $data_cart);
		redirect('admin/data_mahasiswa');
	}

	public function update($id)
	{
		$mahasiswa = $this->model_mahasiswa->cariMahasiswa(trim($this->input->post('NPM')), $id);
		if (count($mahasiswa) > 0) {
			$this->session->set_flashdata("error", "NPM telah ada silahkan masukan NPM yang berbeda");
			redirect("mahasiswa/ubah_mahasiswa/$id");
		} else {

			if (substr(trim(strtoupper($this->input->post('NPM'))), 0, 3) === "G1A") {
				$data_cart = array(
					'NPM' => trim(strtoupper($this->input->post('NPM'))),
					'nama_mahasiswa' => trim($this->input->post('nama_mahasiswa')),
					'tanggal_masuk' => trim($this->input->post('tanggal_masuk')),
					'diubah_tanggal' => date('Y-m-d H:i:s')
				);
				$this->model_mahasiswa->update($id, $data_cart);
				redirect('admin/data_mahasiswa');
			} else {
				$this->session->set_flashdata("error", "NPM harus dimulai dari G1A");
				redirect('admin/ubah_mahasiswa');
			}
		}
	}

	public function delete($id)
	{
		$dataMahasiswa = $this->model_mahasiswa->getMahasiswa($id);
		$this->model_mahasiswa->delete($id);
		$this->model_user->delete($dataMahasiswa[0]->NPM);
		redirect('admin/data_mahasiswa');
	}

	public function data_mahasiswa()
	{
		$data['page'] = 'user/mahasiswa/view_data_mahasiswa';
		$data['data_mahasiswa'] = $this->model_mahasiswa->all();
		$this->load->view('index', $data);
	}

	public function rekap_mahasiswa()
	{
		$data['page'] = 'user/mahasiswa/view_rekap_mahasiswa';
		$data['rekap_mahasiswa'] = $this->model_mahasiswa->rekap_mahasiswa();
		$this->load->view('index', $data);
	}

	public function masa_studi()
	{
		$data['page'] = 'user/mahasiswa/view_masa_studi';
		$data['masa_studi'] = $this->model_mahasiswa->masa_studi();
		$this->load->view('index', $data);
	}

	public function lama_skripsi()
	{
		$data['page'] = 'user/mahasiswa/view_lama_skripsi';
		$data['lama_skripsi'] = $this->model_mahasiswa->lama_skripsi();
		$this->load->view('index', $data);
	}

	public function ubah_mahasiswa($id)
	{
		$data['page'] = 'admin/ubah/ubah_mahasiswa';
		$data["data_mahasiswa"] = $this->model_mahasiswa->getMahasiswa($id);
		$this->load->view('index2', $data);

	}
}

