<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Seminar extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 *        http://example.com/index.php/welcome
	 *    - or -
	 *        http://example.com/index.php/welcome/index
	 *    - or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('form_validation', 'template'));
	}

	public function index()
	{
		$data['page'] = 'user/mahasiswa/view_mahasiwa';
		$this->load->view('index', $data);
	}

	public function create()
	{
		$mahasiswa = $this->model_mahasiswa->cariMahasiswaDenganNPM(trim($this->input->post('NPM')));

		if(count($mahasiswa) > 0 ) {
					$data_cart = array(
						'NPM' => trim($this->input->post('NPM')),
						'judul' => trim($this->input->post('judul')),
						'seminar_proposal' => trim($this->input->post('seminar_proposal')),
						'waktu_proposal' => $this->input->post('waktu_proposal'),
						'ruang_proposal' => $this->input->post('ruang_proposal'),
					);
					$this->model_seminar_proposal->create($data_cart);
					redirect('admin/data_proposal');
		} else {
			$this->session->set_flashdata("error", "NPM tidak ditemukan silahkan masukan NPM yang berbeda");
			redirect("admin/seminar_proposal");
		}

	}

	public function create_hasil()
	{
		$NPM = trim($this->input->post('NPM'));
		$judul = $this->input->post('judul');
		$mahasiswa = $this->model_mahasiswa->cariMahasiswaDenganNPM(trim($this->input->post('NPM')));
		if(count($mahasiswa) > 0 ) {
			if(trim($judul ) !== "" && isset($judul)) {
				$data_cart = array(
					'seminar_hasil' => $this->input->post('seminar_hasil'),
					'judul_hasil' => $judul,
					'waktu_hasil' => $this->input->post('waktu_hasil'),
					'ruang_hasil' => $this->input->post('ruang_hasil'),
					'semhas_diubah_tanggal' => 	date('Y-m-d H:i:s')

				);
				$this->model_seminar_proposal->update($NPM, $data_cart);
				redirect('admin/data_seminar_hasil');
			} else {
				$this->session->set_flashdata("error", "Mahasiswa ini belum melakukan seminar proposal");
				redirect("admin/seminar_hasil");
			}
		} else {
			$this->session->set_flashdata("error", "NPM tidak ditemukan silahkan masukan NPM yang berbeda");
			redirect("admin/seminar_hasil");
		}
	}

	public function create_sidang()
	{
		$NPM = $this->input->post('NPM');
		$judul = $this->input->post('judul');
		$dataMahasiswa = $this->model_mahasiswa->cariMahasiswaDenganNPM($NPM);
		$hitung = 0;
		// Cek apakah NPM ditemukan
		if(isset($dataMahasiswa)) {
			// Cek TOEFL apakah ketiga TOEFL memenuhi syarat
			if(strtotime($dataMahasiswa[0]->tanggal_TOEFL_dibuat) >= strtotime("-1 year", time()) &&
				$dataMahasiswa[0]->TOEFL >= 427){
				$hitung++;
			}
			if(strtotime($dataMahasiswa[0]->tanggal_TOEFL_2_dibuat) >= strtotime("-1 year", time()) &&
				$dataMahasiswa[0]->TOEFL_2 >= 427){
				$hitung++;
			}
			if(strtotime($dataMahasiswa[0]->tanggal_TOEFL_3_dibuat) >= strtotime("-1 year", time()) &&
				$dataMahasiswa[0]->TOEFL_3 >= 427){
				$hitung++;
			}
		} else {
			// Berikan error apabila NPM tidak ditemukan
			$this->session->set_flashdata("error", "Gagal menambahkan sidang dikarenakan NPM tidak ditemukan");
		}

		// Apabila salah satu TOEFL memenuhi syarat maka akan dimasukan data sidang
		if($hitung >0) {
			if(trim($judul ) !== "" && isset($judul)) {
				$data_cart = array(
					'sidang' => $this->input->post('sidang'),
					'judul_sidang' => $judul,
					'waktu_sidang' => $this->input->post('waktu_sidang'),
					'ruang_sidang' => $this->input->post('ruang_sidang'),
					'sidang_diubah_tanggal' => date('Y-m-d H:i:s')
				);
				$this->model_seminar_proposal->update($NPM, $data_cart);
			} else {
				$this->session->set_flashdata("error", "Mahasiswa ini belum melakukan seminar hasil");
				redirect("admin/sidang");
			}
		 } //else {
		// 	// Berikan error apabila ketiga TOEFL tidak ada yang memenuhi syarat
		// 	$this->session->set_flashdata("error","3 TOEFL tidak memenuhi syarat setidaknya diperlukan 1 TOEFL dengan nilai lebih dari 427 dan tidak lebih dari 1 tahun dari waktu tes TOEFL tersebut");
		// }

		redirect('admin/data_sidang');
	}

	public
	function update($id)
	{
		$data_cart = array(
			'NPM' => $this->input->post('NPM'),
			'nama_mahasiswa' => $this->input->post('nama_mahasiswa'),
		);
		$this->model_mahasiswa->update($id, $data_cart);
		redirect('admin/data_mahasiswa');
	}

	public
	function delete($id)
	{
		$this->model_mahasiswa->delete($id);
		redirect('admin/data_mahasiswa');
	}

	public function ambil_judul_skripsi(){
		$npm = $this->input->post("npm");
		$seluruh_judul = $this->model_judul->cariJudulDenganNPM($npm);
		echo json_encode($seluruh_judul);

	}

	public function ambil_judul_semprop(){
		$npm = $this->input->post("npm");
		$seluruh_judul = $this->model_judul->cariJudulSempropDenganNPM($npm);
		echo json_encode($seluruh_judul);

	}

	public function ambil_judul_semhas(){
		$npm = $this->input->post("npm");
		$seluruh_judul = $this->model_judul->cariJudulSemhasDenganNPM($npm);
		echo json_encode($seluruh_judul);

	}

}
