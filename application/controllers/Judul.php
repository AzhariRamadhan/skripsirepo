<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Judul extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('form_validation','template'));
	}
	public function index()
	{
		$data['page'] = 'user/mahasiswa/view_mahasiwa';
		$this->load->view('index', $data);
	}

	public function create(){
		$mahasiswa = $this->model_mahasiswa->cariMahasiswaDenganNPM(trim( $this->input->post('NPM')));

		if(count($mahasiswa) > 0) {
			/*$this->form_validation->set_rules(
				'NPM', 'NPM',
				'required',
				array(
					'required'      => 'NPM di isi terlebih dahulu'
				)
			);

			$this->form_validation->set_rules(
				'judul', 'judul',
				'required',
				array(
					'required'      => 'Judul di isi terlebih dahulu'
				)
			);


			if ($this->form_validation->run() == FALSE)
			{
				red
			}*/
			$data_cart = array(

				'NPM' => $this->input->post('NPM'),
				'judul' => $this->input->post('judul_proposal'),
				'pembimbing1' => '-',
				'pembimbing2' => '-',
				'penguji1' => '-',
				'penguji2' => '-',
			);

			$this->model_judul->create($data_cart);
			redirect('admin/data_judul');
		} else {
			$this->session->set_flashdata("error", "NPM tidak ditemukan silahkan masukan NPM yang berbeda");
			redirect("admin/input_judul");
		}
	}

	public function create_hasil(){
		$data_cart = array(
               'NPM'     	=> $this->input->post('NPM'),
               'judul'    	=> $this->input->post('judul_proposal'),
               'pembimbing1'    	=> '-',
               'pembimbing2'    	=> '-',
               'penguji1'    	=> '-',
               'penguji2'    	=> '-',
            );	
		$this->model_judul->create($data_cart);
		redirect('admin/data_judul');
	}

	public function create_sidang(){
		$NPM = $this->input->post('NPM');
		$data_cart = array(
               'seminar_hasil'  => $this->input->post('seminar_hasil'),
               'judul_hasil'    => $this->input->post('judul_hasil'),
               'waktu_hasil'  	=> $this->input->post('waktu_hasil'),
               'ruang_hasil'    => $this->input->post('ruang_hasil'),
            );	
		$this->model_seminar_proposal->update($NPM, $data_cart);
		$data_cart = array(
               'judul'    		=> $this->input->post('judul_proposal'),
               'pembimbing1'    => $this->input->post('pembimbing1'),
               'pembimbing2'    => $this->input->post('pembimbing2'),
               'penguji1'    	=> $this->input->post('penguji1'),
               'penguji2'    	=> $this->input->post('penguji2'),
            );	
		$this->model_judul->update($NPM, $data_cart);
		redirect('admin/data_seminar_hasil');
	}

	public function update($id){
		$data_cart = array(
               'NPM'     	=> $this->input->post('NPM'),
               'nama_mahasiswa'    	=> $this->input->post('nama_mahasiswa'),
            );	
		$this->model_mahasiswa->update($id, $data_cart);
		redirect('admin/data_mahasiswa');	}

	public function delete($id){
		$this->model_mahasiswa->delete($id);
		redirect('admin/data_mahasiswa');
	}

}
