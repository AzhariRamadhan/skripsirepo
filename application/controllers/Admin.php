<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('form_validation','Template'));
	}
	public function index()
	{
		$data['page'] = 'admin/main_menu';
		$this->load->view('index2', $data);
	}

	//Input Judul
	public function input_judul()
	{
		$data['page'] = 'admin/input/input_judul';
		$this->load->view('index2', $data);
	}

	public function input_mahasiswa()
	{
		$data['page'] = 'admin/input/input_mahasiswa';
		$this->load->view('index2', $data);
	}

	public function input_dosen()
	{
		$data['page'] = 'admin/input/input_dosen';
		$this->load->view('index2', $data);
	}



	public function input_ipk()
	{
		$data['page'] = 'admin/seminar/input_ipk';
		$this->load->view('index2', $data);
	}

	public function input_toefl()
	{
		$data['page'] = 'admin/seminar/input_toefl';
		$this->load->view('index2', $data);
	}

	public function input_dosen_pp()
	{
		$data['page'] = 'admin/input/input_dosen_pp';
		$this->load->view('index2', $data);
	}

	public function judul()
	{
		$data['page'] = 'admin/seminar/judul';
		$data['data_judul'] =  $this->model_judul->rekap_judul();
		$this->load->view('index2', $data);
	}

	//Seminar
	public function seminar_proposal()
	{
		$data['page'] = 'admin/seminar/seminar_proposal';
		$this->load->view('index2', $data);
	}

	public function seminar_hasil()
	{
		$data['page'] = 'admin/seminar/seminar_hasil';
		$data['dosen'] =  $this->model_dosen->all();
		$this->load->view('index2', $data);
	}

	public function sidang()
	{
		$data['page'] = 'admin/seminar/sidang';
		$data['dosen'] =  $this->model_dosen->all();
		$this->load->view('index2', $data);
	}

	//Master DATA
	public function data_mahasiswa()
	{
		$data['page'] = 'admin/data/data_mahasiswa';
		$data['data_mahasiswa'] =  $this->model_mahasiswa->detail_mahasiswa();
		$this->load->view('index2', $data);
	}

	public function data_dosen()
	{
		$data['page'] = 'admin/data/data_dosen';
		$data['data_dosen'] =  $this->model_dosen->dataDosen();
		$this->load->view('index2', $data);
	}



	public function data_dibimbing($nip){
		$data["page"] = 'admin/data/data_dibimbing';
		$data["data_dibimbing"] = $this->model_dosen->dataDibimbing($nip);
		$data["nama"] = $this->model_dosen->get_nama_dosen($nip);
		$this->load->view('index2', $data);
	}

	public function data_diuji($nip){
		$data["page"] = 'admin/data/data_diuji';
		$data["data_diuji"] = $this->model_dosen->dataDiuji($nip);
		$data["nama"] = $this->model_dosen->get_nama_dosen($nip);
		$this->load->view('index2', $data);
	}

	public function data_judul()
	{
		$data['page'] = 'admin/data/data_judul';
		$data['data_judul'] =  $this->model_judul->rekap_judul();
		$this->load->view('index2', $data);
	}

	public function data_proposal()
	{
		$data['page'] = 'admin/data/data_proposal';
		$data['data_proposal'] =  $this->model_seminar_proposal->all();
		$this->load->view('index2', $data);
	}	
	public function Verifikasi($npm, $status)
	{
        
		$this->load->model('model_seminar_proposal');
		if($status == 1){
			$status = 'Sudah';
		}else{
			$status = 'Belum';
		}
		$data_anggota = array (
			'Verifikasi'	=> $status
		);
		$this->model_seminar_proposal->update($npm, $data_anggota);
		redirect('Admin/data_proposal');
	}
	public function data_seminar_hasil()
	{
		$data['page'] = 'admin/data/data_seminar_hasil';
		$data['data_seminar_hasil'] =  $this->model_seminar_proposal->all();
		$this->load->view('index2', $data);
	}

	public function data_sidang()
	{
		$data['page'] = 'admin/data/data_sidang';
		$data['data_sidang'] =  $this->model_seminar_proposal->all();
		$this->load->view('index2', $data);
	}

	public function data_dosen_pp()
	{
		$this->load->model('model_dosen');
		$data = array(
	       'page'   => 'admin/data/data_dosen_pp',
	       'data_dosen_pp'  => $this->model_dosen->pp()
	     );
		$this->load->view('index2', $data);
	}

	//Cetak
	public function cetak_proposal()
	{
		$data['page'] = 'admin/cetak/cetak_proposal';
		$data['cetak_proposal'] =  $this->model_judul->tgl_proposal();
		$this->load->view('index2', $data);
	}

	public function undangan_proposal($tgl)
	{
		$data['page'] = 'admin/cetak/undangan_proposal';
		$data['cetak_proposal'] =  $this->model_judul->cetak_proposal($tgl);
		$data['tgl'] = $tgl;
		$this->load->view('blank', $data);
	}

	public function cetak_hasil()
	{
		$data['page'] = 'admin/cetak/cetak_hasil';
		$data['cetak_hasil'] =  $this->model_seminar_hasil->all();
		$this->load->view('index2', $data);
	}

	public function undangan_hasil($tgl)
	{
		$data['page'] = 'admin/cetak/undangan_hasil';
		$data['cetak_Hasil'] =  $this->model_judul->cetak_hasil($tgl);
		$data['cetak_dosen'] = $this->model_dosen->cetak_hasil($tgl);
		$data['tgl'] = $tgl;
		$this->load->view('blank', $data);
	}

	public function cetak_sidang()
	{
		$data['page'] = 'admin/cetak/cetak_sidang';
		$data['cetak_sidang'] =  $this->model_sidang->all();
		$this->load->view('index2', $data);
	}

	public function undangan_sidang($tgl)
	{
		$data['page'] = 'admin/cetak/undangan_sidang';
		$data['cetak_Sidang'] =  $this->model_judul->cetak_sidang($tgl);
		$data['cetak_dosen'] = $this->model_dosen->cetak_hasil($tgl);
		$data['tgl'] = $tgl;
		$this->load->view('blank', $data);
	}

	/*public function setKaprodi($id){
		$dataDosen = $this->model_dosen->detailDosen($id);

		if($dataDosen[0]->Apakah_kaprodi == "ya"){
			$this->model_dosen->update($id, array(
				"Apakah_kaprodi" => "tidak"));
			$this->model_user->update($dataDosen[0]->NIP, array(
				"Status" => "Dosen"
			));
		} else {
			$this->model_dosen->update($id, array(
				"Apakah_kaprodi" => "ya"));
			$this->model_user->update($dataDosen[0]->NIP, array(
				"Status" => "Kaprodi"
			));
		}
		redirect("Admin/data_dosen");
	}*/

	public function rekap_akhir()
	{
		$data['page'] = 'admin/rekap/rekap_akhir';
		$data['data_mahasiswa'] =  $this->model_rekap->rekap_akhir();
		$this->load->view('index2', $data);
	}


}
