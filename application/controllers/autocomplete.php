<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Autocomplete extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
	}
	public function search()
	{
		// tangkap variabel keyword dari URL
		$keyword = $this->uri->segment(3);

		// cari di database
		$data = $this->db->from('tb_mahasiswa')->like('NPM',$keyword)->get();	

		// format keluaran di dalam array
		foreach($data->result() as $row)
		{
			$arr['query'] = $keyword;
			$arr['suggestions'][] = array(
				'value'	=>$row->NPM,
				'nama_mahasiswa'	=>$row->nama_mahasiswa

			);
		}
		// minimal PHP 5.2
		echo json_encode($arr);
	}

	public function pp()
	{
		// tangkap variabel keyword dari URL
		$keyword = $this->uri->segment(3);

		// cari di database
		$data = $this->db->query("SELECT tb_mahasiswa.NPM, tb_mahasiswa.nama_mahasiswa, tb_sempro.judul, 
          (select Nama_Dosen from tb_dosen where tb_dosen.NIP=tb_sempro.pembimbing1) as pembimbing1, 
          (select Nama_Dosen from tb_dosen where tb_dosen.NIP=tb_sempro.pembimbing2) as pembimbing2, 
          (select Nama_Dosen from tb_dosen where tb_dosen.NIP=tb_sempro.penguji1) as penguji1, 
          (select Nama_Dosen from tb_dosen where tb_dosen.NIP=tb_sempro.penguji2) as penguji2 FROM  
          tb_mahasiswa JOIN tb_sempro on tb_mahasiswa.NPM = tb_sempro.NPM ");	

		// format keluaran di dalam array
		foreach($data->result() as $row)
		{
			$arr['query'] = $keyword;
			$arr['suggestions'][] = array(
				'value'	=>$row->NPM,
				'pembimbing1'	=>$row->pembimbing1,
				'pembimbing2'	=>$row->pembimbing2,
				'penguji1'	=>$row->penguji1,
				'penguji2'	=>$row->penguji2,

			);
		}
		// minimal PHP 5.2
		echo json_encode($arr);
	}

	public function dosenPembimbing(){
			// tangkap variabel keyword dari URL
			$keyword = $this->uri->segment(3);

			// cari di database
			$data = $this->db->query("SELECT tb_mahasiswa.NPM, tb_mahasiswa.nama_mahasiswa, tb_sempro.judul, 
          (select Nama_Dosen from tb_dosen where tb_dosen.NIP=tb_sempro.pembimbing1) as pembimbing1, 
          (select Nama_Dosen from tb_dosen where tb_dosen.NIP=tb_sempro.pembimbing2) as pembimbing2, 
          (select Nama_Dosen from tb_dosen where tb_dosen.NIP=tb_sempro.penguji1) as penguji1, 
          (select Nama_Dosen from tb_dosen where tb_dosen.NIP=tb_sempro.penguji2) as penguji2,
           tb_sempro.pembimbing1 as nipPembimbing1,
			tb_sempro.pembimbing2 as nipPembimbing2,
           tb_sempro.penguji1 as nipPenguji1,
           tb_sempro.penguji2 as nipPenguji2,
            tb_sempro.judul as judul FROM  
          tb_mahasiswa JOIN tb_sempro on tb_mahasiswa.NPM = tb_sempro.NPM ");

			// format keluaran di dalam array
			foreach($data->result() as $row)
			{
				$arr['query'] = $keyword;
				$arr['suggestions'][] = array(
					'value'	=>$row->NPM,
					'pembimbing1'	=>$row->pembimbing1,
					'pembimbing2'	=>$row->pembimbing2,
					'penguji1'	=>$row->penguji1,
					'penguji2'	=>$row->penguji2,
					'nipPembimbing1'	=>$row->nipPembimbing1,
					'nipPembimbing2'	=>$row->nipPembimbing2,
					'nipPenguji1'	=>$row->nipPenguji1,
					'nipPenguji2'	=>$row->nipPenguji2,
					'judul'			=>$row->judul

				);
			}
			// minimal PHP 5.2
			echo json_encode($arr);


	}

	public function pps()
	{
		// tangkap variabel keyword dari URL
		$keyword = $this->uri->segment(3);

		// cari di database
		$data = $this->db->from('tb_sempro')->like('NPM',$keyword)->get();	

		// format keluaran di dalam array
		foreach($data->result() as $row)
		{
			$arr['query'] = $keyword;
			$arr['suggestions'][] = array(
				'value'	=>$row->NPM,
				'pembimbing1'	=>$row->pembimbing1,
				'pembimbing2'	=>$row->pembimbing2,
				'penguji1'	=>$row->penguji1,
				'penguji2'	=>$row->penguji2,

			);
		}
		// minimal PHP 5.2
		echo json_encode($arr);
	}

	public function search1()
	{
		// tangkap variabel keyword dari URL
		$keyword = $this->uri->segment(3);

		// cari di database
		$data = $this->db->from('tb_dosen')->like('Nama_Dosen',$keyword)->get();	

		// format keluaran di dalam array
		foreach($data->result() as $row)
		{
			$arr['query'] = $keyword;
			$arr['suggestions'][] = array(
				'value'	=>$row->Nama_Dosen,
				'NIP'	=>$row->NIP

			);
		}
		// minimal PHP 5.2
		echo json_encode($arr);
	}

		public function judul()
	{
		// tangkap variabel keyword dari URL
		$keyword = $this->uri->segment(3);

		// cari di database
		$data = $this->db->from('tb_sempro')->like('NPM',$keyword)->get();	

		// format keluaran di dalam array
		foreach($data->result() as $row)
		{
			$arr['query'] = $keyword;
			$arr['suggestions'][] = array(
				'value'	=>$row->NPM,
				'judul'	=>$row->judul

			);
		}
		// minimal PHP 5.2
		echo json_encode($arr);
	}

	public function cariNamaDosen() {
		// tangkap variabel keyword dari URL
		$keyword = $this->uri->segment(3);

		// cari di database
		$data = $this->db->from('tb_dosen')->like('NIP',$keyword)->get();

		// format keluaran di dalam array
		foreach($data->result() as $row)
		{
			$arr['query'] = $keyword;
			$arr['suggestions'][] = array(
				'value'	=>$row->NIP,
				'Nama_Dosen'	=>$row->Nama_Dosen

			);
		}
		// minimal PHP 5.2
		echo json_encode($arr);
	}

	public function search2()
	{
		// tangkap variabel keyword dari URL
		$keyword = $this->uri->segment(3);

		// cari di database
		$data = $this->db->from('tb_sempro')->like('NPM',$keyword)->get();	

		// format keluaran di dalam array
		foreach($data->result() as $row)
		{
			$arr['query'] = $keyword;
			$arr['suggestions'][] = array(
				'value'	=>$row->pembimbing1,
				'NPM'	=>$row->NPM

			);
		}
		// minimal PHP 5.2
		echo json_encode($arr);
	}
}
?>
