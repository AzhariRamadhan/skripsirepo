<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('form_validation','Template'));
	}
	public function index()
	{
		$data['page'] = 'admin/main_menu';
		$this->load->view('index', $data);
	}

	public function Dosen()
	{
		$data['page'] = 'user/dosen/view_dosen';
		$data['dosen'] =  $this->model_dosen->all();
		$this->load->view('index', $data);
	}

	public function Mahasiswa()
	{
		$data['page'] = 'user/mahasiswa/view_mahasiswa';
		$this->load->view('index', $data);
	}

	public function Judul()
	{
		$data['page'] = 'user/judul/view_judul';
		$data['judul'] =  $this->model_judul->rekap_judul();
		$this->load->view('index', $data);
	}

	public function Jadwal()
	{
		$data['page'] = 'user/jadwal/view_jadwal';
		$data['jadwal'] =  $this->model_judul->rekap_judulskripsi();
		$this->load->view('index', $data);
	}	

	public function login()
	{
		
		$this->load->view('login');
	}

}
