<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
     
    public function index(){ //1 menampilkan view login
        $session = $this->session->userdata('Login'); 
        if(empty($session)){
            $this->load->view('Login');
        }else{
            if ($this->session->userdata('Status')=="Admin") {
                redirect("Admin/index");
            }else{
                redirect("member/dashboard");
            }
        }
    }
    public function cek_login(){ // 2 setelah memasukkan input masuk ke fungsi cek_login
        $this->load->model('model_login'); //memanggil model_login
        $npm=$_POST["npm"]; //menyimpan inputan dari nama ke variabel $nama
        $password=$_POST["password"]; //menyimpan inputan dari password ke variabel $password
        $cek=$this->model_login->cek_user($npm, md5($password)); //cek nama dan password ke model_login
        if(count($cek)==1){ //jika datanya sesuai dengan data di database
            $this->session->set_userdata(array(//menyimpan data ke session
                'login'=>true,
                'Status' => $cek[0]['Status'],
            ));
            if ($cek[0]['Status']=="Admin") {
                redirect("admin");
                
            } else if($cek[0]['Status']=="Dosen"){
					redirect("dosen");
            } else if($cek[0]['Status']=="Mahasiswa"){
                redirect("mahasiswa");
        /*    } else if($cek[0]['Status']=="Kaprodi") {
            	redirect("kaprodi");*/
			} else{
                redirect("Welcome");
            }
            
        } else { //jika berbeda dengan data di database
            $this->session->set_flashdata('notif',
                '<div class="alert alert-danger" role="alert"> 
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                Username atau Password anda salah </div>');
            redirect('login','refresh');
        }
    }
    public function logout(){
        $this->session->sess_destroy();
        redirect('login','refresh');
    }
    
}
