<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dosen extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('form_validation','Template'));
	}

    public function index(){
        $data["page"] = "dosen/main_menu";
        $this->load->view("dosen/index", $data);
    }




    //=================================================================================================================//

	public function index_beranda()
	{
		$data['page'] = 'user/dosen/view_dosen';
		$this->load->view('index', $data);
	}


	public function create(){
		$data_cart = array(
               'NIP'     	=> trim($this->input->post('NIP')),
               'nama_dosen'    	=> trim($this->input->post('nama_dosen')),
               'Fakultas'   => trim($this->input->post('Fakultas')),
               'Prodi'   => trim($this->input->post('Prodi')),
            );	
		$this->model_dosen->create($data_cart);
		redirect('admin/data_dosen');
	}

	public function create_pp(){
            $npm =  trim($this->input->post('NPM'));
		$data_cart = array(
				'judul'   => $this->input->post('judul'),
               'pembimbing1'   => $this->input->post('pembimbing1'),
               'pembimbing2'   	=> $this->input->post('pembimbing2'),
               'penguji1'   	=> $this->input->post('penguji1'),
               'penguji2'   	=> $this->input->post('penguji2'),
            );	
		$this->model_dosen->create_pp($npm, $data_cart);
		redirect('admin/data_dosen_pp');
	}

	public function update($id){
		$dosen = $this->model_dosen->cariDosen(trim($this->input->post('NIP')), $id);

		if(count($dosen) > 0){
			$this->session->set_flashdata("error", "NIP telah ada silahkan masukan NIP yang berbeda");
			redirect("dosen/ubah_dosen/$id");
		} else {
			$data_cart = array(
				'NIP' => trim($this->input->post('NIP')),
				'nama_dosen' => trim($this->input->post('nama_dosen')),
				'Fakultas' => trim($this->input->post('Fakultas')),
				'Prodi' => trim($this->input->post('Prodi')),
			);
			$this->model_dosen->update($id, $data_cart);
			redirect('admin/data_dosen');
		}

	}

	public function delete($id){
		$this->model_dosen->delete($id);
		redirect('admin/data_dosen');
	}

	public function data_dosen()
	{
		$data['page'] = 'user/dosen/view_data_dosen';
		$data['data_dosen'] =  $this->model_dosen->all();
		$this->load->view('index', $data);
	}

	public function rekap_dosen()
	{
		$data['page'] = 'user/dosen/view_rekap_dosen';
		$data['rekap_dosen'] =  $this->model_dosen->all();
		$this->load->view('index', $data);
	}

	public function masa_studi()
	{
		$data['page'] = 'user/dosen/view_masa_studi';
		$data['masa_studi'] =  $this->model_dosen->masa_studi();
		$this->load->view('index', $data);
	}

	public function lama_skripsi()
	{
		$data['page'] = 'user/dosen/view_lama_skripsi';
		$data['lama_skripsi'] =  $this->model_dosen->lama_skripsi();
		$this->load->view('index', $data);
	}

	public function rekap_bimbing_uji($nip,$status)
  {
     $data = array(
      'isi' => 'tabel_daftarbimbingan',
      'status' => $status,
      'dosen' => $this->model_admin->get_nama_dosen($nip),
      'data'  => $this->model_admin->rekap_bimbingan($nip,$status)
     );
    $this->load->view('layout',$data);
  }

	public function input_dosen()
	{
		$data['page'] = 'admin/input/input_dosen';
		$this->load->view('index2', $data);
	}

	public function ubah_dosen($id)
	{
		$data['page'] = 'admin/ubah/ubah_dosen';
		$data["data_dosen"] = $this->model_dosen->detailDosen($id);
		$this->load->view('index2', $data);
	}

}
