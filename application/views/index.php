<html>

<title>Sistem Informasi Informatika</title>
	<head>
		<link href="<?php echo base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet">
		<link rel="shortcut icon" href="<?php echo base_url("");?>" type="image/jpg">
		<link rel="stylesheet" href="<?php echo base_url()."assets/font-awesome/css/font-awesome.min.css"; ?>">
		<link rel="stylesheet" href="<?php echo base_url()."assets/css/plugins/dataTables/dataTables.bootstrap.css"; ?>">
		<link rel="stylesheet" href="<?php echo base_url()."assets/table/media/css/jquery.dataTables.css"; ?>">
		<link rel="stylesheet" href="<?php echo base_url()."assets/table/media/css/jquery.dataTables.min.css"; ?>">		
        <script src="<?php echo base_url('assets/js/jquery-1.8.3.min.js');?>"></script>
        <script src="<?php echo base_url('assets/js/bootstrap.js');?>"></script>
        <script src="<?php echo base_url('assets/table/media/js/jquery.dataTables.min.js');?>"></script>
	<body style="background-color: #caeff2">
		<?php $status=$this->session->userdata('Status');?>
		<?php if ($status=="Admin") {?>
		<?php $this->load->view('template/header')?>
		<div class='col-md-2' >
			<?php $this->load->view('template/sidebar')?>
		</div>
		<div class='col-md-10'>
			<div class="panel panel-default" >
			<?php $this->load->view($page); ?>
			</div>
		</div>
		<?php } else {?>
		
		<?php $this->load->view('template/header_member')?>
		<div class="panel panel-default" >
			<?php $this->load->view($page); ?>
		</div>
		<?php }?>
		<script>
     	$(document).ready(function(){
        $('#myTable').DataTable();
      	});
		</script>
	</body>
	
</html>
<div class="col-md-12">
<footer>
  <p>&copy; PPL TI UNIB 2017 Company, Inc. &middot; <a href="#">Privacy</a> &middot; <a href="#">Terms</a></p>
</footer>
</div>