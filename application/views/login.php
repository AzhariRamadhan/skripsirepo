<html>
	<head> 
	<title>Login </title>
		<meta charset="UTF-8">
		<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
		<link rel="shortcut icon" href="<?php echo base_url()."assets/images/logo_icon.png";?>" type="image/png">
	    <link rel="stylesheet" href="<?php echo base_url()."assets/css/login.css"; ?>">
	    <link rel="stylesheet" href="<?php echo base_url()."assets/fonts/css/font-awesome.min.css"; ?>">
	    <link rel="stylesheet" href="<?php echo base_url()."assets/css/bootstrap.min.css";?>">
	    <link rel="stylesheet" href="<?php echo base_url()."assets/css/bootstrap-reset.css";?>">
	    <style type="text/css">
	    .akun{
    	color: #black
	    }
	    .akun:hover{
	    	color:#black;
	    	-webkit-transition-duration: 0.25s;
	    	transition-duration: 0.25s;
	    }
	    </style>
	</head>
	<body style="background:#48d">
		<div class="container" >
			<div class="badan" style="top:20px;position:relative">
				<h1 style="text-align:center; color: #fff">Login</h1>
				<?=$this->session->flashdata('notif')?>
					<form class="form-signin" action="<?php echo base_url();?>index.php/login/cek_login" method="post">
						  <div class="form-group">
						    <label class="sr-only" for="exampleInputEmail3">NPM</label>
						    <input name="npm" class="form-control" id="exampleInputEmail3" placeholder="Username" required="">
						  </div>
						  <div class="form-group">
						    <label class="sr-only" for="exampleInputPassword3">Password</label>
						    <input type="password" name="password" class="form-control" id="exampleInputPassword3" placeholder="Password" required="">
						  </div>
						  <button type="submit" class="btn btn-default">Masuk</button>
					</form>
			</div>
		</div>
	</body>
</html>
