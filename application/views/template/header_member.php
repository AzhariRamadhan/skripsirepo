<nav class="navbar navbar-inverse">
	<div class="container-fluid">
      		<ul class="nav navbar-nav">
        <li>
          	<a href="<?php echo site_url('Welcome/index');?>" ><span class="glyphicon glyphicon-home" aria-hidden="true"></span> Beranda<span style="color: #fff"class="sr-only"></span></a>
        </li>
        <li>
          	<a href="<?php echo site_url('Welcome/Dosen');?>" ><span class="fa fa-user-md" aria-hidden="true"></span> Dosen<span style="color: #fff"class="sr-only"></span></a>
        </li>
        <li>
          	<a href="<?php echo site_url('Welcome/Mahasiswa');?>" ><span class="fa fa-users" aria-hidden="true"></span> Mahasiswa<span style="color: #fff"class="sr-only"></span></a>
        </li>
        <li>
          	<a href="<?php echo site_url('Welcome/Judul');?>" ><span class="fa fa-list-alt" aria-hidden="true"></span> Judul Skripsi<span style="color: #fff"class="sr-only"></span></a>
        </li>
        <li>
          	<a href="<?php echo site_url('Welcome/Jadwal');?>" ><span class="fa fa-th-list" aria-hidden="true"></span> Jadwal Seminar<span style="color: #fff"class="sr-only"></span></a>
        </li>
      		</ul>
          <ul class="nav navbar-nav navbar-right">
          <li>
            <a href="<?php echo site_url('Welcome/Login');?>" ><span class="fa fa-left" aria-hidden="true"></span> Login<span style="color: #fff"class="sr-only"></span></a>
        </li>
          </ul>  
    </div>
</nav>
