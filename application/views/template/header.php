<nav class="navbar navbar-inverse" >
  	<div class="container-fluid">
    	<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      		
          <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" style="color: #fff" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $this->session->userdata('nama')?><span class="caret"></span></a>
                <ul class="dropdown-menu">
                                    <li>
                                        <a href="<?php echo site_url('welcome/bantuan') ?>">Bantuan</a>
                                    </li>
                <li><a href="<?php echo site_url('login/logout');?>"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                </ul>
            </li>
          </ul>  
    	</div><!-- /.navbar-collapse -->
  	</div><!-- /.container-fluid -->
</nav>
