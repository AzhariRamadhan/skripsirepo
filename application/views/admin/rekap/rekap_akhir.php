<div class="panel-heading"><h1><span class="label label-default">Data Mahasiswa</span></h1></div>
<div class="panel-body">
	<table id="myTable" class="table table-striped table-bordered table-hover" style="background-color:#fff">
		<thead>
		<tr>
			<th>NPM</th>
			<th>Nama Mahasiswa</th>
			<th>Lama skripsi</th>
			<th>Lama studi</th>

		</tr>
		</thead>
		<tbody>
		<?php
		foreach($data_mahasiswa as $data_mahasiswas) :
			$data['data_mahasiswas']=$data_mahasiswas ?>
			<tr>
				<td><?=$data_mahasiswas->NPM?></td>
				<td><?=$data_mahasiswas->nama_mahasiswa?></td>
				<td><?php
					if(floor($data_mahasiswas->lama_skripsi / 12) > 0){
						echo floor($data_mahasiswas->lama_skripsi / 12) . " Tahun, ";
					}
					echo $data_mahasiswas->lama_skripsi % 12 . " Bulan";

				?></td>
				<td><?php
					if(floor($data_mahasiswas->lama_studi / 12) > 0){
						echo floor($data_mahasiswas->lama_studi / 12) . " Tahun, ";
					}
					echo $data_mahasiswas->lama_studi % 12 . " Bulan";

					?></td>
			</tr>
		<?php endforeach; ?>
		</tbody>
	</table>
</div>
