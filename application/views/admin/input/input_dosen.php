  <div class="col-md-12">
    <div class="panel panel-default">
    <div class="panel-heading"><h1><span class="label label-info">Input Dosen</span></h1></div>
      <div class="panel-body">
          <?= form_open_multipart('dosen/create',['class'=>'form-horizontal'])?>
                    <div class="col-md-12">
                    <div class="form-group">
                        <div class="col-md-3">
                        <label for="inputEmail3" class="">NIP</label>
                        </div>
                        <div class="col-sm-6">
                            <input type="search"  class='form-control'  name="NIP"/>
                      </div>
                    </div>
                    </div>
                    <div class="col-md-12">         
                    <div class="form-group">
                        <div class="col-md-3">
                        <label for="inputPassword3" class="">Nama Dosen</label>
                        </div>
                        <div class="col-sm-6">
                            <input type="text"  class='form-control'  name="nama_dosen"/>
                      </div>
                    </div>
                    </div>
                    <div class="col-md-12">         
                    <div class="form-group">
                        <div class="col-md-3">
                        <label for="inputPassword3" class="">Fakultas</label>
                        </div>
                        <div class="col-sm-6">
                            <input type="text"  class='form-control'  name="Fakultas"/>
                      </div>
                    </div>
                    </div>
                    <div class="col-md-12">         
                    <div class="form-group">
                        <div class="col-md-3">
                        <label for="inputPassword3" class="">Prodi</label>
                        </div>
                        <div class="col-sm-6">
                            <input type="text"  class='form-control'  name="Prodi"/>
                      </div>
                    </div>
                    </div>                        
                    <div class="form-group last">
                        <div class="col-sm-offset-8 col-sm-9">
                            <button type="submit" class="btn btn-primary">Simpan</button>
                            <button type="reset" class="btn btn-default">Kosongkan</button>
                        </div>
                    </div>  
                    
          <?= form_close()?>
    </div>
    </div>
    </div>


