<div class="panel panel-default">
	<div class="panel-heading"><h1><span class="label label-info">Input Mahasiswa</span></h1></div>
	<div class="panel-body">
		<?= form_open_multipart('Mahasiswa/create', ['class' => 'form-horizontal']) ?>
		<div class="col-md-12">
			<div class="form-group">
				<div class="col-md-3">
					<label for="inputEmail3" class="col-sm-3 control-label">NPM</label>
				</div>
				<div class="col-sm-3">
					<input type="search" class='form-control' id="autocomplete1" name="NPM"/>
				</div>
			</div>
		</div>
		<div class="col-md-12">
			<div class="form-group">
				<div class="col-md-3">
					<label for="inputPassword3" class="col-sm-3 control-label">Nama Mahasiswa</label>
				</div>
				<div class="col-sm-3">
					<input type="text" class='form-control' id="v_nama" name="nama_mahasiswa"/>
				</div>
			</div>
		</div>

		<div class="col-md-12">
			<div class="form-group">
				<div class="col-md-3">
					<label>Tanggal Masuk</label>
				</div>
				<div class="col-md-4 ">
					<input type="date" class="form-control" name="tanggal_masuk" required>
				</div>
			</div>
		</div>

		<div class="col-md-12">

			<div class="form-group last">
				<div class="col-sm-offset-8 col-sm-9">
					<button type="submit" class="btn btn-primary">Simpan</button>
					<button type="reset" class="btn btn-default">Kosongkan</button>
				</div>
			</div>

			<?= form_close() ?>
		</div>
	</div>
</div>


