<div id="modal_edit_<?php echo $data_dosen_pps->NPM;?>" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
<script type='text/javascript'>
        var site = "<?php echo site_url();?>";
        $(function(){
            $('.autocomplete').autocomplete({
                // serviceUrl berisi URL ke controller/fungsi yang menangani request kita
                serviceUrl: site+'/autocomplete/search',
                // fungsi ini akan dijalankan ketika user memilih salah satu hasil request
                onSelect: function (suggestion) {
                    $('#v_nama').val(''+suggestion.nama_mahasiswa); // membuat id 'v_nama' untuk ditampilkan
                }
            });
        });
        $(function(){
            $('.autocomplete').autocomplete({
                // serviceUrl berisi URL ke controller/fungsi yang menangani request kita
                serviceUrl: site+'/autocomplete/judul',
                // fungsi ini akan dijalankan ketika user memilih salah satu hasil request
                onSelect: function (suggestion) {
                    $('#v_judul').val(''+suggestion.judul); // membuat id 'v_nama' untuk ditampilkan
                }
            });
        });
        $(function(){
            $('.autocomplet').autocomplete({
                // serviceUrl berisi URL ke controller/fungsi yang menangani request kita
                serviceUrl: site+'/autocomplete/search1',
                // fungsi ini akan dijalankan ketika user memilih salah satu hasil request
                onSelect: function (suggestion) {
                    $('#v_namadosen').val(''+suggestion.NIP); // membuat id 'v_nama' untuk ditampilkan
                }
            });
        });
        $(function(){
            $('.autocomplets').autocomplete({
                // serviceUrl berisi URL ke controller/fungsi yang menangani request kita
                serviceUrl: site+'/autocomplete/search1',
                // fungsi ini akan dijalankan ketika user memilih salah satu hasil request
                onSelect: function (suggestion) {
                    $('#v_namadosens').val(''+suggestion.NIP); // membuat id 'v_nama' untuk ditampilkan
                }
            });
        });
        $(function(){
            $('.autocompletss').autocomplete({
                // serviceUrl berisi URL ke controller/fungsi yang menangani request kita
                serviceUrl: site+'/autocomplete/search1',
                // fungsi ini akan dijalankan ketika user memilih salah satu hasil request
                onSelect: function (suggestion) {
                    $('#v_namadosenss').val(''+suggestion.NIP); // membuat id 'v_nama' untuk ditampilkan
                }
            });
        });
        $(function(){
            $('.autocompletsss').autocomplete({
                // serviceUrl berisi URL ke controller/fungsi yang menangani request kita
                serviceUrl: site+'/autocomplete/search1',
                // fungsi ini akan dijalankan ketika user memilih salah satu hasil request
                onSelect: function (suggestion) {
                    $('#v_namadosensss').val(''+suggestion.NIP); // membuat id 'v_nama' untuk ditampilkan
                }
            });
        });
    </script>
    <div class="panel panel-default">
    <div class="panel-heading"><h1><span class="label label-info">Input Dosen Pembimbing dan Penguji</span></h1></div>
      <div class="panel-body">
          <?= form_open_multipart('Dosen/create_pp',['class'=>'form-horizontal'])?>
          <div class="col-md-12">
          <div class="form-group">
          <div class="col-md-3">
          <label for="inputEmail3" class="">NPM</label>
          </div>
          <div class="col-sm-3">
          <input type="search" class='autocomplete nama' id="autocomplete1" name="NPM"/>
          </div>
          </div>
          </div>
          <div class="col-md-12">         
          <div class="form-group">
          <div class="col-md-3">
          <label for="inputPassword3" class="">Nama Mahasiswa</label>
          </div>
          <div class="col-sm-3">
          <input type="text" class='autocomplete' id="v_nama" name="nama_mahasiswa"/>
          </div>
          </div>
          </div>
          <div class="col-md-12">
          <div class="form-group">
          <div class="col-md-3">
          <label for="inputEmail3" class="">Judul</label>
          </div>
          <div class="col-sm-3">
          <input type="text"  class='autocompletee' id="v_judul" name="judul"  />
          </div>
          </div>
          </div>
          <div class="col-md-12">
          <div class="form-group">
          <div class="col-md-3">
          <label for="inputEmail3" class="">Pebimbing 1</label>
          </div>
          <div class="col-sm-3">
          <input type="search" class='autocomplet nama' id="autocomplete1" name=""/>
          </div>
          <div class="col-sm-3">
          <input type="text" class='autocomplet' id="v_namadosen" name="pembimbing1"/>
          </div>
          </div>
          </div>
          <div class="col-md-12">
          <div class="form-group">
          <div class="col-md-3">
          <label for="inputEmail3" class="">Pembimbing 2</label>
          </div>
          <div class="col-sm-3">
          <input type="search" class='autocomplets nama' id="autocomplete1" name=""/>
          </div>
          <div class="col-sm-3">
          <input type="text" class='autocomplets' id="v_namadosens" name="pembimbing2"/>
          </div>
          </div>
          </div>
          <div class="col-md-12">
          <div class="form-group">
          <div class="col-md-3">
          <label for="inputEmail3" class="">Penguji 1</label>
          </div>
          <div class="col-sm-3">
          <input type="search" class='autocompletss nama' id="autocomplete1" name=""/>
          </div>
          <div class="col-sm-3">
          <input type="text" class='autocompletss' id="v_namadosenss" name="penguji1"/>
          </div>
          </div>
          </div>
          <div class="col-md-12">
          <div class="form-group">
          <div class="col-md-3">
          <label for="inputEmail3" class="">Penguji 2</label>
          </div>
          <div class="col-sm-3">
          <input type="search" class='autocompletsss nama' id="autocomplete1" name=""/>
          </div>
          <div class="col-sm-3">
          <input type="text" class='autocompletsss' id="v_namadosensss" name="penguji2"/>
          </div>
          </div>
          </div>
          <div class="col-md-12">                               
          <div class="form-group last">
            <div class="col-sm-offset-8 col-sm-9">
              <button type="submit" class="btn btn-primary">Submit Button</button>
              <button type="reset" class="btn btn-default">Reset Button</button>
            </div>
          </div>                 
          <?= form_close()?>
    </div>
    </div>
    </div>
</div>
</div>
</div>
</div>