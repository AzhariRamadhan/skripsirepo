<script type='text/javascript'>
    var site = "<?php echo site_url();?>";
    $(function () {
        $('.autocomplete').autocomplete({
            // serviceUrl berisi URL ke controller/fungsi yang menangani request kita
            serviceUrl: site + '/autocomplete/search',
            // fungsi ini akan dijalankan ketika user memilih salah satu hasil request
            onSelect: function (suggestion) {
                $('#v_nama').val('' + suggestion.nama_mahasiswa); // membuat id 'v_nama' untuk ditampilkan

            }
        });
    });
    $(function () {
        $('.autocomplete').autocomplete({
            // serviceUrl berisi URL ke controller/fungsi yang menangani request kita
            serviceUrl: site + '/autocomplete/judul',
            // fungsi ini akan dijalankan ketika user memilih salah satu hasil request
            onSelect: function (suggestion) {
                $('#v_judul').val('' + suggestion.judul); // membuat id 'v_nama' untuk ditampilkan
				console.log("======================");
                $.ajax({
                    url:"<?php echo base_url();?>/Seminar/ambil_judul_skripsi",
                    data: {npm:document.getElementById("autocomplete1").value},
                    dataType:'text',
                    type:"POST",
                    success: function(result){
                        var json = JSON.parse(result);
                        $("#seluruh_judul").empty();
                       json.forEach(function(data){

                           var select = document.getElementById("seluruh_judul");
                           var opt = document.createElement('option');
                           opt.appendChild( document.createTextNode(data["judul"]) );
                           opt.value = data["judul"];
                           select.appendChild(opt);
                           console.log("======================");
					   });

                    }
                });
            }
        });
    });




</script>
<div class="panel panel-default">
	<div class="panel-heading"><h1><span class="label label-info">Seminar Proposal</span></h1></div>
	<div class="panel-body">
		<?= form_open_multipart('Seminar/create', ['class' => 'form-horizontal']) ?>
		<div class="col-md-12">
			<div class="form-group">
				<div class="col-md-3">
					<label for="inputEmail3" class="">NPM</label>
				</div>
				<div class="col-sm-3">
					<input type="search" class='autocomplete nama' id="autocomplete1" name="NPM" required/>
				</div>
			</div>
		</div>
		<div class="col-md-12">
			<div class="form-group">
				<div class="col-md-3">
					<label for="inputEmail3" class="">Judul</label>
				</div>
				<div class="col-sm-3">
					<select name ="judul" id= "seluruh_judul" class="form-control">
						<option>Masukan NPM terlebih dahulu</option>

					</select>
			</div>
		</div>
		<div class="col-md-12">
			<div class="form-group">
				<div class="col-md-3">
					<label>Tanggal Seminar Proposal</label>
				</div>
				<div class="col-md-4 ">
					<input type="date" class="form-control" name="seminar_proposal" required>
				</div>
			</div>
		</div>
		<div class="col-md-12">
			<div class="form-group">
				<div class="col-md-3">
					<label>Waktu</label>
				</div>
				<div class="col-md-4 ">
					<input type="time" class="form-control" name="waktu_proposal" required>
				</div>
			</div>
		</div>
		<div class="col-md-12">
			<div class="form-group">
				<div class="col-md-3">
					<label>Ruangan</label>
				</div>
				<div class="col-md-4">
					<input type="text" class="form-control" name="ruang_proposal" required>
				</div>
			</div>
		</div>

		<div class="col-md-12">
			<div class="form-group last">
				<div class="col-sm-offset-8 col-sm-9">
					<button type="submit" class="btn btn-primary">Simpan</button>
					<button type="reset" class="btn btn-default">Kosongkan</button>
				</div>
			</div>
			<?= form_close() ?>
		</div>
	</div>
</div>
