<script type='text/javascript'>
    var site = "<?php echo site_url();?>";
    $(function () {
        $('.autocomplete').autocomplete({
            serviceUrl: site + '/autocomplete/search',
            onSelect: function (suggestion) {
                $('#v_nama').val('' + suggestion.nama_mahasiswa); // membuat id 'v_nama' untuk ditampilkan
            }
        });
    });
</script>
<div class="col-md-12">
	<div class="panel panel-default">
		<div class="panel-heading"><h1><span class="label label-info">Input Rekap IPK</span></h1></div>
		<div class="panel-body">
			<?= form_open_multipart('Mahasiswa/create_toefl', ['class' => 'form-horizontal']) ?>
			<div class="col-md-12">
				<div class="form-group">
					<div class="col-md-3">
						<label for="inputEmail3" class="col-sm-3 control-label">NPM</label>
					</div>
					<div class="col-sm-3">
						<input type="search" class='autocomplete nama' id="autocomplete1" name="NPM"/>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<div class="col-md-3">
						<label for="inputPassword3" class="col-sm-3 control-label">Nama Mahasiswa</label>
					</div>
					<div class="col-sm-3">
						<input type="text" class='autocomplete' id="v_nama" name="nama_mahasiswa"/>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<div class="col-md-3">
						<label for="inputPassword3" class="col-sm-3 control-label">IPK</label>
					</div>
					<div class="col-xs-1">
						<input class="form-control" type="text" name="IPK" required>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<div class="col-md-3">
						<label for="inputPassword3" class="col-sm-3 control-label">TOEFL 1</label>
					</div>
					<div class="col-xs-1">
						<input class="form-control" type="text" name="TOEFL">
					</div>
				</div>
			</div>

			<div class="col-md-12">
				<div class="form-group">
					<div class="col-md-3">
						<label for="inputPassword3" class="col-sm-3 control-label">TOEFL 2</label>
					</div>
					<div class="col-xs-1">
						<input class="form-control" type="text" name="TOEFL_2">
					</div>
				</div>
			</div>

			<div class="col-md-12">
				<div class="form-group">
					<div class="col-md-3">
						<label for="inputPassword3" class="col-sm-3 control-label">TOEFL 3</label>
					</div>
					<div class="col-xs-1">
						<input class="form-control" type="text" name="TOEFL_3" >
					</div>
				</div>
			</div>

			<div class="col-md-12">
				<div class="form-group">
					<div class="col-md-3">
						<label>Tanggal TOEFL</label>
					</div>
					<div class="col-xs-3">
						<input type="date" class="form-control" name="tanggal_TOEFL_dibuat" >
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<div class="col-md-3">
						<label>Tanggal TOEFL 2</label>
					</div>
					<div class="col-xs-3">
						<input type="date" class="form-control" name="tanggal_TOEFL_2_dibuat">
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<div class="col-md-3">
						<label>Tanggal TOEFL 3</label>
					</div>
					<div class="col-xs-3">
						<input type="date" class="form-control" name="tanggal_TOEFL_3_dibuat">
					</div>
				</div>
			</div>
			<div class="form-group last">
				<div class="col-sm-offset-8 col-sm-9">
					<button type="submit" class="btn btn-primary">Simpan</button>
					<button type="reset" class="btn btn-default">Kosongkan</button>
				</div>
			</div>
			<?= form_close() ?>
		</div>
	</div>
</div>
