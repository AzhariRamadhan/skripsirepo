<div class="panel-heading"><h1><span class="label label-default">Data dibimbing oleh <?php echo $nama[0]->Nama_Dosen; ?></span></h1></div>
	<div class="panel-body">
			<table id="myTable" class="table table-striped table-bordered table-hover" style="background-color:#fff">
				<thead>
					<tr>
						<th>NPM</th>
						<th>Judul</th>
					</tr>
				</thead>
				<tbody>
						<?php
						foreach($data_dibimbing as $data) {?>
						<tr>
							<td><?=$data->NPM?></td>
							<td><?=$data->judul?></td>
						</tr>
						<?php } ?>
				</tbody>
			</table>
		</div>
