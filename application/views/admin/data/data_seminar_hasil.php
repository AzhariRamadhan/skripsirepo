<div class="panel-heading"><h1><span class="label label-default">Data Seminar Hasil</span></h1></div>
	<div class="panel-body">
			<table class="table table-striped table-bordered table-hover tabel-semhas" style="background-color:#fff">
				<thead>
					<tr>
						<th>NPM</th>
						<th>Nama</th>
						<th>Seminar Seminar Hasil</th>
						<th>Judul Seminar Hasil</th>
						<th>Waktu</th>
						<th>Ruang</th>
						<th>Diubah tanggal</th>
					</tr>
				</thead>
				<tbody>
						<?php
						foreach($data_seminar_hasil as $data_seminar_hasils) :
						$data['data_seminar_hasils']=$data_seminar_hasils ?>
						<tr>
							<td><?=$data_seminar_hasils->npm?></td>
							<td><?=$data_seminar_hasils->nama_mahasiswa?></td>
							<td><?=$data_seminar_hasils->seminar_hasil?></td>
							<td><?=$data_seminar_hasils->judul_hasil?></td>							
							<td><?=$data_seminar_hasils->waktu_hasil?></td>
							<td><?=$data_seminar_hasils->ruang_hasil?></td>
							<td data-order="<?=$data_seminar_hasils->semhas_diubah_tanggal?>"><?php echo date('d-m-Y H:i:s',strtotime($data_seminar_hasils->semhas_diubah_tanggal));?></td>
						</tr>
						<?php endforeach; ?>
				</tbody>
			</table>
		</div>
