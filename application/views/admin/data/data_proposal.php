<div class="panel-heading"><h1><span class="label label-default">Data proposal</span></h1></div>
	<div class="panel-body">
			<table class="table table-striped table-bordered table-hover tabel-semprop" style="background-color:#fff">
				<thead>
					<tr>
						<th>NPM</th>
						<th>Nama</th>
						<th>Seminar proposal</th>
						<th>Judul Proposal</th>
						<th>Waktu</th>
						<th>Ruang</th>
						<th>Diubah tanggal</th>
					</tr>
				</thead>
				<tbody>
						<?php
						foreach($data_proposal as $data_proposals) :{
						$data['data_proposals']=$data_proposals ?>
						<tr>
							<td><?=$data_proposals->npm?></td>
							<td><?=$data_proposals->nama_mahasiswa?></td>
							<td><?=$data_proposals->seminar_proposal?></td>
							<td><?=$data_proposals->judul?></td>							
							<td><?=$data_proposals->waktu_proposal?></td>
							<td><?=$data_proposals->ruang_proposal?></td>
							<td data-order="<?=$data_proposals->semprop_diubah_tanggal?>"><?php echo date('d-m-Y H:i:s',strtotime($data_proposals->semprop_diubah_tanggal));?></td>




							<?php } ?>
						<?php endforeach; ?>
				</tbody>
			</table>
		</div>
