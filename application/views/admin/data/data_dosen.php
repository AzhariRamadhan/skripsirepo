<div class="panel-heading"><h1><span class="label label-default">Data Dosen</span></h1></div>
	<div class="panel-body">
			<table id="myTable" class="table table-striped table-bordered table-hover" style="background-color:#fff">
				<thead>
					<tr>
						<th>NIP</th>
						<th>Nama Dosen</th>
						<th>Fakultas</th>
						<th>Prodi</th>
					<!--	<th>Kaprodi</th>-->
						<th>Jumlah bimbingan</th>
						<th>Jumlah diuji</th>
						<th>Aksi</th>
					</tr>
				</thead>
				<tbody>
						<?php
						foreach($data_dosen as $data_dosens) :
						$data['data_dosens']=$data_dosens ?>
						<tr>
							<td><?=$data_dosens->NIP?></td>
							<td><?=$data_dosens->Nama_Dosen?></td>
							<td><?=$data_dosens->Fakultas?></td>							
							<td><?=$data_dosens->Prodi?></td>
							<td><a href="<?php echo base_url();?>Admin/data_dibimbing/<?php echo $data_dosens->NIP; ?>"><?=$data_dosens->jumlah_dibimbing?></a></td>
							<td><a href="<?php echo base_url();?>Admin/data_diuji/<?php echo $data_dosens->NIP; ?>"><?=$data_dosens->jumlah_diuji?></a></td>
						<!--	<td><?/*=$data_dosens->Apakah_kaprodi*/?></td>-->
							<td><div class="row">
									<div class="col-md-6 col-sm-6">

										<a href="<?php echo base_url() ?>Dosen/ubah_dosen/<?php echo $data_dosens->id_dosen ?>">
											<i class="fa fa-pencil-square-o"></i></a>

									</div>
									<div class="col-md-6 col-sm-6">

										<a href="<?php echo base_url() ?>Dosen/delete/<?php echo $data_dosens->id_dosen ?>">
											<i class="fa fa-trash-o "></i></a>

									</div>
								</div></td>
						</tr>
						<?php endforeach; ?>
				</tbody>
			</table>
		</div>
