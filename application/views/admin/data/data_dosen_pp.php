<div class="panel-heading"><h1><span class="label label-default">Data Dosen Pembimbing dan Penguji</span></h1></div>
	<div class="panel-body">
			<table id="myTable" class="table table-striped table-bordered table-hover" style="background-color:#fff">
				<thead>
					<tr>
						<th>NPM</th>
						<th>Nama Mahasiswa</th>
						<th>Judul Proposal</th>
						<th>Pembimbing 1</th>
						<th>Pembimbing 2</th>
						<th>Penguji 1</th>
						<th>Penguji 2</th>
											
					</tr>
				</thead>
				<tbody>
						<?php
						foreach($data_dosen_pp as $data_dosen_pps) :
						$data['data_dosen_pps']=$data_dosen_pps ?>
						<tr>
							<td><?=$data_dosen_pps->NPM?></td>
							<td><?=$data_dosen_pps->nama_mahasiswa?></td>
							<td><?=$data_dosen_pps->judul?></td>
							
							<td><?=$data_dosen_pps->pembimbing1?></td>
							<td><?=$data_dosen_pps->pembimbing2?></td>
							<td><?=$data_dosen_pps->penguji1?></td>
							<td><?=$data_dosen_pps->penguji2?></td>

							
							
						</tr>
						<?php endforeach; ?>
				</tbody>
			</table>
		</div>
		