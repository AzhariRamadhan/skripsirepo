<div class="panel-heading"><h1><span class="label label-default">Data Mahasiswa</span></h1></div>
<div style="margin-top:20px"></div>
<!-- <form action="<?php echo base_url();?>Mahasiswa/upload/" method="post" enctype="multipart/form-data">
    <input type="file" name="file"/>
    <input type="submit" value="Upload file"/>
</form> -->
	<div class="panel-body">
			<table class="table table-striped table-bordered table-hover tabel-mahasiswa" style="background-color:#fff">
				<thead>
					<tr>
						<th>NPM</th>
						<th>Nama Mahasiswa</th>
						<th>Tanggal Masuk</th>
						<th>Judul Proposal</th>
						<th>IPK</th>
						<th>TOEFL 1</th>
						<th>TOEFL 2</th>
						<th>TOEFL 3</th>
						<th>Tanggal TOEFL 1</th>
						<th>Tanggal TOEFL 2</th>
						<th>Tanggal TOEFL 3</th>
						<th>Sidang</th>
						<th>Diubah tanggal</th>
						<th>Aksi</th>
											
					</tr>
				</thead>
				<tbody>
						<?php
						foreach($data_mahasiswa as $data_mahasiswas) :
						$data['data_mahasiswas']=$data_mahasiswas ?>
						<tr>
							<td><?=$data_mahasiswas->NPM?></td>
							<td><?=$data_mahasiswas->nama_mahasiswa?></td>
							<td><?=$data_mahasiswas->tanggal_masuk?></td>
							<td><?=$data_mahasiswas->judul?></td>							
							<td><?=$data_mahasiswas->IPK?></td>
							<td><?=$data_mahasiswas->TOEFL?></td>
							<td><?=$data_mahasiswas->TOEFL_2?></td>
							<td><?=$data_mahasiswas->TOEFL_3?></td>
							<td><?=$data_mahasiswas->tanggal_TOEFL_dibuat?></td>
							<td><?=$data_mahasiswas->tanggal_TOEFL_2_dibuat?></td>
							<td><?=$data_mahasiswas->tanggal_TOEFL_3_dibuat?></td>
							<td><?=$data_mahasiswas->sidang?></td>
							<td data-order="<?=$data_mahasiswas->diubah_tanggal?>"><?php echo date('d-m-Y H:i:s',strtotime($data_mahasiswas->diubah_tanggal));?></td>
							<td><div class="row">
									<div class="col-md-6 col-sm-6">

											<a href="<?php echo base_url() ?>Mahasiswa/ubah_mahasiswa/<?php echo $data_mahasiswas->id_mahasiswa ?>">
												<i class="fa fa-pencil-square-o"></i></a>

									</div>
									<div class="col-md-6 col-sm-6">

										<a href="<?php echo base_url() ?>Mahasiswa/delete/<?php echo $data_mahasiswas->id_mahasiswa ?>">
											<i class="fa fa-trash-o "></i></a>

									</div>
								</div></td>
						</tr>
						<?php endforeach; ?>
				</tbody>
			</table>
		</div>
