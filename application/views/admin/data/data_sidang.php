<div class="panel-heading"><h1><span class="label label-default">Data sidang</span></h1></div>
	<div class="panel-body">
			<table class="table table-striped table-bordered table-hover tabel-sidang" style="background-color:#fff">
				<thead>
					<tr>
						<th>NPM</th>
						<th>Nama</th>
						<th>Sidang</th>
						<th>Judul sidang</th>
						<th>Waktu</th>
						<th>Ruang</th>
						<th>Diubah tanggal</th>
					</tr>
				</thead>
				<tbody>
						<?php
						foreach($data_sidang as $data_sidangs) :
						$data['data_sidangs']=$data_sidangs ?>
						<tr>
							<td><?=$data_sidangs->npm?></td>
							<td><?=$data_sidangs->nama_mahasiswa?></td>
							<td><?=$data_sidangs->sidang?></td>
							<td><?=$data_sidangs->judul_sidang?></td>							
							<td><?=$data_sidangs->waktu_sidang?></td>
							<td><?=$data_sidangs->ruang_sidang?></td>
							<td data-order="<?=$data_sidangs->sidang_diubah_tanggal?>"><?php echo date('d-m-Y H:i:s',strtotime($data_sidangs->sidang_diubah_tanggal));?></td>
						</tr>
						<?php endforeach; ?>
				</tbody>
			</table>
		</div>
