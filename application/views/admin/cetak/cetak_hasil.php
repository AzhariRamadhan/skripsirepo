<div class="panel-heading"><h1><span class="label label-default">Cetak hasil</span></h1></div>
	<div class="panel-body">
			<table id="myTable" class="table table-striped table-bordered table-hover" style="background-color:#fff">
				<thead>
					<tr>
						<th>No</th>
						<th>Seminar hasil</th>
						<th></th>				
					</tr>
				</thead>
				<tbody>
						<?php $no = 1;
						foreach($cetak_hasil as $cetak_hasils) :
						$cetak['cetak_hasils']=$cetak_hasils ?>
						<tr>
							<td><?= $no++?></td>
							<td><?=$cetak_hasils->nama_mahasiswa?></td>
							<td><a target="_blank" href="<?php echo site_url('admin/undangan_hasil/'. $cetak_hasils->NPM)?>">Cetak</a></td>			
						</tr>
						<?php endforeach; ?>
				</tbody>
			</table>
		</div>