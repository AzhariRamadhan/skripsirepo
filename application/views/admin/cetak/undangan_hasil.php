<?php 
function tanggal_indo($tanggal, $cetak_hari = false)
            {
              $hari = array ( 1 =>    'Senin',
                    'Selasa',
                    'Rabu',
                    'Kamis',
                    'Jumat',
                    'Sabtu',
                    'Minggu'
                  );
                  
              $bulan = array (1 =>   'Januari',
                    'Februari',
                    'Maret',
                    'April',
                    'Mei',
                    'Juni',
                    'Juli',
                    'Agustus',
                    'September',
                    'Oktober',
                    'November',
                    'Desember'
                  );
              $split    = explode('-', $tanggal);
              $tgl_indo = $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
              
              if ($cetak_hari) {
                $num = date('N', strtotime($tanggal));
                return $hari[$num] . ', ' . $tgl_indo;
              }
              return $tgl_indo;
            }?>
<html>
	<head>		
		<title>Seminar hasil</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link href="<?php echo base_url('assets/css/bootstrap.min.css')?>" rel="stylesheet">
		<style type="text/css" media="print">
   			 @page { size: potrait;  }
		</style>
	</head>
	<body onload="javascript:window.print()">
    <img class= "img-responsive" style="float:left; width:120;height:120;margin-right:-100;" src=<?php echo base_url('assets/img/logo-unib.png');?>>
    <center>
      <h5>KEMENTRIAN RISET, TEKNOLOGI DAN PENDIDIKAN TINGGI</h5>
      <h4>UNIVERSITAS BENGKULU<br>FAKULTAS TEKNIK<br>PROGRAM STUDI TEKNIK INFORMATIKA</h4>
      <h5>JL.W.R.SUPRATMAN KANDANG LIMUN</h5>
    <h6>Tlp.+62736-344087,21170 EXT. 277. Fax +62736-349134 Laman: www.informatika.ft.ac.id</h6>
    </center>
   <hr style="border-style:solid;border-color:black;border-width:3;">
    <div class ="row">
      <div class="col-xs-12">
        <div class="col-xs-6">
                <p>No : ___/UN.30.13.5/TU/2017 <br>Hal: Undangan Seminar Hasil Skripsi</p>
        </div>
        <div class="col-xs-6 text-right">
                <p>Tanggal : <?php
            $tanggal = date('Y-m-d', strtotime($tgl));
            echo tanggal_indo($tanggal, true);
            ?></p>
        </div>
        <div class="col-xs-12">
                <p>Kepada Yth. 
                <?php $no=1;
                foreach($cetak_dosen as $data){?>                  
                  <br>1. <?php echo $data->pembimbing1 ;?>
                  <br>2. <?php echo $data->pembimbing2 ;?>
                  <br>3. <?php echo $data->penguji2 ;?>
                  <br>4. <?php echo $data->penguji1 ;?>                 
                 <?php }?>
                 <br>ditempat</p>
        </div>
        <div class="col-xs-12">
                <p>Sehubung dengan akan dilaksanakannya Seminar Hasil mahasiswa a.n:</p>
        </div>   
      
    </div>
    <div class="col-xs-12">
    <table class="table ">
      <thead>
        <th>No</th>
        <th>NPM</th>
        <th>Nama</th>
        <th>Judul</th>
        <th>Waktu</th>
        <th>Ruang</th>
      </thead>
      
        <?php $no=1;
        foreach($cetak_Hasil as $data){?>
          <tr>
          <td><?php echo $no++ ;?></td>
          <td><?php echo $data->npm?></td>
          <td><?php echo $data->nama_mahasiswa ?></td>
          <td><?php echo $data->judul_hasil?></td>
          <td><?php echo $data->waktu_hasil?></td>
          <td><?php echo $data->ruang_hasil?></td>
          </tr>
         <?php }?>        
    </table>
    </div>
    <div class="col-xs-12">
                <p>Kami Mengharapkan kehadiran Bapak/Ibu dalam seminar Hasil Skripsi Mahasiswa tersebut sebagai penguji pada:
                  <br> Hari,Tanggal:<?php
            // FUNGSI
            
            $tanggal = date('Y-m-d', strtotime($tgl));
            echo tanggal_indo($tanggal, true);
            ?></p>
        </div>
        <div class="col-xs-12">
                <p>Kehadiran Bapak/Ibu tepat waktu sangat kami harapkan. Khususnya Kepada Bapak/Ibu Dosen Prodi Teknik Informatika perlu kami sampaikan, bahwa Pembimbing Utama akan ditetapkan langsung setelah pelaksanaan Seminar Hasil. Atas perhatian Bapak/Ibu diucapkan terima kasih</p>
        </div>
        <div class="col-xs-offset-7 col-xs-5">
                <p>Mengetahui,<br>Ketua Prodi Teknik Informatika<br><br><br><br><br><br>Ernawati,ST.,M.Cs<br>NIP.197308142006042001<br></p>
        </div>
      </div> 

	</body>
</html>