<?php 
function tanggal_indo($tanggal, $cetak_hari = false)
						{
							$hari = array ( 1 =>    'Senin',
										'Selasa',
										'Rabu',
										'Kamis',
										'Jumat',
										'Sabtu',
										'Minggu'
									);
									
							$bulan = array (1 =>   'Januari',
										'Februari',
										'Maret',
										'April',
										'Mei',
										'Juni',
										'Juli',
										'Agustus',
										'September',
										'Oktober',
										'November',
										'Desember'
									);
							$split 	  = explode('-', $tanggal);
							$tgl_indo = $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
							
							if ($cetak_hari) {
								$num = date('N', strtotime($tanggal));
								return $hari[$num] . ', ' . $tgl_indo;
							}
							return $tgl_indo;
						}?>
<div class="panel-heading"><h1><span class="label label-default">Cetak proposal</span></h1></div>
	<div class="panel-body">
			<table id="myTable" class="table table-striped table-bordered table-hover" style="background-color:#fff">
				<thead>
					<tr>
						<th>No</th>
						<th>Seminar proposal</th>
						<th>Jumlah</th>
						<th></th>				
					</tr>
				</thead>
				<tbody>
						<?php $no = 1;
						foreach($cetak_proposal as $cetak_proposals) :
						$cetak['cetak_proposals']=$cetak_proposals ?>
						<tr>
							<td><?= $no++?></td>
							<td><?php
						// FUNGSI
						
						$tanggal = date('Y-m-d', strtotime($cetak_proposals->seminar_proposal));
						echo tanggal_indo($tanggal, true);
						?>	</td>
							<td><?=count($this->model_judul->cetak_proposal($cetak_proposals->seminar_proposal));?></td>
							<td><a target="_blank" href="<?php echo site_url('admin/undangan_proposal/'. $cetak_proposals->seminar_proposal)?>">Cetak</a></td>			
						</tr>
						<?php endforeach; ?>
				</tbody>
			</table>
		</div>