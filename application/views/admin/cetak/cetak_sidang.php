<div class="panel-heading"><h1><span class="label label-default">Cetak sidang</span></h1></div>
	<div class="panel-body">
			<table id="myTable" class="table table-striped table-bordered table-hover" style="background-color:#fff">
				<thead>
					<tr>
						<th>No</th>
						<th>Sidang</th>
						<th></th>				
					</tr>
				</thead>
				<tbody>
						<?php $no = 1;
						foreach($cetak_sidang as $cetak_sidangs) :
						$cetak['cetak_sidangs']=$cetak_sidangs ?>
						<tr>
							<td><?= $no++?></td>
							<td><?=$cetak_sidangs->nama_mahasiswa?></td>
							<td><a target="_blank" href="<?php echo site_url('admin/undangan_sidang/'. $cetak_sidangs->NPM)?>">Cetak</a></td>			
						</tr>
						<?php endforeach; ?>
				</tbody>
			</table>
		</div>