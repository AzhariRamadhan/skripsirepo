<div class="col-md-12">
	<div class="panel panel-default">
		<div class="panel-heading"><h1><span class="label label-info">Input Dosen</span></h1></div>
		<div class="panel-body">

			<?php foreach ($data_dosen as $dosen) { ?>
				<?= form_open_multipart('dosen/update/' . $dosen->id_dosen,['class'=>'form-horizontal'])?>
			<div class="col-md-12">
				<div class="form-group">
					<div class="col-md-3">
						<label for="inputEmail3" class="">NIP</label>
					</div>
					<div class="col-sm-6">
						<input type="search"  class='form-control'  name="NIP" value="<?php echo $dosen->NIP; ?>"/>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<div class="col-md-3">
						<label for="inputPassword3" class="">Nama Dosen</label>
					</div>
					<div class="col-sm-6">
						<input type="text"  class='form-control'  name="nama_dosen" value="<?php echo $dosen->Nama_Dosen; ?>"/>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<div class="col-md-3">
						<label for="inputPassword3" class="">Fakultas</label>
					</div>
					<div class="col-sm-6">
						<input type="text"  class='form-control'  name="Fakultas" value="<?php echo $dosen->Fakultas; ?>"/>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<div class="col-md-3">
						<label for="inputPassword3" class="">Prodi</label>
					</div>
					<div class="col-sm-6">
						<input type="text"  class='form-control'  name="Prodi" value="<?php echo $dosen->Prodi; ?>"/>
					</div>
				</div>
			</div>
			<div class="form-group last">
				<div class="col-sm-offset-8 col-sm-9">
					<button type="submit" class="btn btn-primary">Ubah</button>
					<button type="reset" class="btn btn-default">Kosongkan</button>
				</div>
			</div>
				<?= form_close()?>
			<?php } ?>

		</div>
	</div>
</div>


