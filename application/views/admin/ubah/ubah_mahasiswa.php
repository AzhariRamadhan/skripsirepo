
<div class="panel panel-default">
	<div class="panel-heading"><h1><span class="label label-info">Ubah Mahasiswa</span></h1></div>
	<div class="panel-body">
		<?= form_open_multipart('Mahasiswa/update/' . $data_mahasiswa[0]->id_mahasiswa,['class'=>'form-horizontal'])?>

		<div class="col-md-12">
			<div class="form-group">

				<div class="col-md-3">
					<label for="inputEmail3" class="col-sm-3 control-label">NPM</label>
				</div>
				<div class="col-sm-3">
					<input type="search"  class='form-control' id="autocomplete1" name="NPM" value="<?php echo
					$data_mahasiswa[0]->NPM; ?>"/>

				</div>
			</div>

				<input type="hidden" name="id" value="<?php echo
				$data_mahasiswa[0]->id_mahasiswa; ?>>

		</div>
		<div class="col-md-12">
			<div class="form-group">
				<div class="col-md-3">
					<label for="inputPassword3" class="col-sm-3 control-label">Nama Mahasiswa</label>
				</div>
				<div class="col-sm-3">
					<input type="text"  class='form-control' id="v_nama" name="nama_mahasiswa" value="<?php echo
					$data_mahasiswa[0]->nama_mahasiswa; ?>"/>
				</div>
			</div>
		</div>
		<div class="col-md-12">

			<div class="form-group last">
				<div class="col-sm-offset-8 col-sm-9">
					<button type="submit" class="btn btn-primary">Ubah</button>
				</div>
			</div>

			<?= form_close()?>
		</div>
	</div>
</div>


