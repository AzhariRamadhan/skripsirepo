<div align="center" style="background-color: #caeff2">
<p class="wadah-mengetik" >Sistem Informasi Pengolahan Data Skripsi</p>
<p class="wadah-mengetik" >Teknik Informatika</p>
<div class="panel panel-default" >
<div class="carousel-inner" role="listbox">
  <div class="item active">
    <img class="third-slide" src="<?php echo base_url('assets/img/logo.png')?>"  >
  </div>
 </div>
</div>
<div class="panel panel-default" style="background-color: #caeff2">
  <div class="container marketing">

      <!-- Three columns of text below the carousel -->
      <div class="row">
        <div class="col-lg-4">
          <img class="img-circle" src="<?php echo base_url('assets/img/bem.png')?>" alt="Generic placeholder image" width="140" height="140">
          <h2>Profil Prodi</h2>
          <p>Melihat profil Teknik Informatika.</p>
          <p><a class="btn btn-default" href="<?php echo base_url('Welcome/ProfilTi')?>" role="button">View &raquo;</a></p>
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
          <img class="img-circle" src="<?php echo base_url('assets/img/himatif.png')?>" alt="Generic placeholder image" width="140" height="140">
          <h2>Profil Himatif</h2>
          <p>Organisasi himpunana Mahasiswa Teknik Informatika Universitas Bengkulu.</p>
          <p><a class="btn btn-default" href="<?php echo base_url('Welcome/test1')?>" role="button">View &raquo;</a></p>
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
          <img class="img-circle" src="<?php echo base_url('assets/img/2.png')?>" alt="Generic placeholder image" width="140" height="140">
          <h2>Contact</h2>
          <p>Kontak untuk menghubungi pihak prodi Teknik Informatika.</p>
          <p><a class="btn btn-default" href="<?php echo base_url('Welcome/test')?>" role="button">View &raquo;</a></p>
        </div><!-- /.col-lg-4 -->
      </div><!-- /.row -->



      <!-- FOOTER -->
      <footer>
        <p class="pull-right"><a href="<?php echo base_url ('Welcome/index2')?>">Back to top</a></p>
        <p>&copy; PPL TI UNIB 2017. &middot; <a href="#">Privacy</a> &middot; <a href="#">Terms</a></p>
      </footer>

    </div>
</div>
</div>
<style type="text/css">
  
.wadah-mengetik
{
  font-size: 22px;
  width: 740px;
  white-space:nowrap;
  overflow:hidden;
  -webkit-animation: ketik 5s steps(50, end);
  animation: ketik 5s steps(50, end);
}

@keyframes ketik{
    from { width: 0; }
}

@-webkit-keyframes ketik{
    from { width: 0; }
}
</style>