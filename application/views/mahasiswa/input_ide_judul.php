<script type='text/javascript'>
    var site = "<?php echo site_url();?>";
    $(function () {
        $('.autocomplete').autocomplete({
            // serviceUrl berisi URL ke controller/fungsi yang menangani request kita
            serviceUrl: site + '/autocomplete/search',
            // fungsi ini akan dijalankan ketika user memilih salah satu hasil request
            onSelect: function (suggestion) {
                $('#v_nama').val('' + suggestion.nama_mahasiswa); // membuat id 'v_nama' untuk ditampilkan
            }
        });
    });

</script>
<div class="col-md-12">
	<div class="panel panel-default">
		<div class="panel-heading"><h1><span class="label label-info">Input Judul</span></h1></div>
		<div class="panel-body">
			<?= form_open_multipart('Judul/create', ['class' => 'form-horizontal']) ?>
			<div class="col-md-12">
			</div>


			<div class="col-md-12">
				<div class="form-group">
					<div class="col-md-3">
						<label for="inputPassword3" class="col-sm-3 control-label">Judul Proposal</label>
					</div>
					<div class="col-sm-6">
						<input class="form-control" type="text" name="judul_proposal" required minlength="1" maxlength="255">
					</div>
				</div>
			</div>
			<div class="form-group last">
				<div class="col-sm-offset-8 col-sm-9">
					<button type="submit" class="btn btn-primary">Simpan</button>
					<button type="reset" class="btn btn-default">Kosongkan</button>
				</div>
			</div>

			<?= form_close() ?>
		</div>
	</div>
</div>


