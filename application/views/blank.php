<html>

<title>Sistem Informasi Informatika</title>
	<head>
		<link href="<?php echo base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet">
		<link rel="shortcut icon" href="<?php echo base_url("");?>" type="image/jpg">
		<link rel="stylesheet" href="<?php echo base_url()."assets/font-awesome/css/font-awesome.min.css"; ?>">
		<link rel="stylesheet" href="<?php echo base_url()."assets/css/plugins/dataTables/dataTables.bootstrap.css"; ?>">
		<link rel="stylesheet" href="<?php echo base_url()."assets/table/media/css/jquery.dataTables.css"; ?>">
		<link rel="stylesheet" href="<?php echo base_url()."assets/table/media/css/jquery.dataTables.min.css"; ?>">	
		<link href='<?php echo base_url();?>assets/js/jquery.autocomplete.css' rel='stylesheet' />	
        <script src="<?php echo base_url('assets/js/jquery-1.8.3.min.js');?>"></script>
		<script type='text/javascript' src='<?php echo base_url();?>assets/js/jquery.autocomplete.js'></script>
        <script src="<?php echo base_url('assets/js/bootstrap.js');?>"></script>
        <script src="<?php echo base_url('assets/table/media/js/jquery.dataTables.min.js');?>"></script>
	<body >
		<div class="col-md-10 col-md-offset-2">
		<div class="panel panel-default" >
			<?php $this->load->view($page); ?>
		</div>
		</div>
		<script>
     	$(document).ready(function(){
        $('#myTable').DataTable();
      	});
		</script>
	</body>
	
</html>
