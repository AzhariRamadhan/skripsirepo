<div class="panel-heading"><h1><span class="label label-default">Data Mahasiswa</span></h1></div>
	<div class="panel-body">
			<table id="myTable" class="table table-striped table-bordered table-hover" style="background-color:#fff">
				<thead>
					<tr>
						<th>ID</th>
						<th>Nama</th>
						<th>IPK</th>
					</tr>
				</thead>
				<tbody>
						<?php foreach($data_mahasiswa as $row) :
						$data['row']=$row ?>
						<tr>
							<td><?=$row->NPM?></td>
							<td><?=$row->nama_mahasiswa?></td>
							<td><?=$row->IPK?></td>							
						</tr>
						<?php endforeach; ?>
				</tbody>
			</table>
	</div>