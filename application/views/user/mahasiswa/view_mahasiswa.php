<div class="container">
    <h1 style="text-align: center"> Selamat Datang Pada Halaman Mahasiswa</h1>
    <h2 style="text-align: center">Program Studi Teknik Informatika Universitas Bengkulu</h2>
        <div class="row">
          <div class="col-lg-3">
            <img class="img-circle" src="<?php echo base_url('assets/img/1.png')?>" alt="Generic placeholder image" width="140" height="140">
            <h2>Data Mahasiswa</h2>
            <p>Data Mahasiswa Teknik Informatika yang mengajukan mata kuliah skripsi</p>
            <p><a class="btn btn-default" href="<?php echo site_url('Mahasiswa/data_mahasiswa')?>" role="button">Lihat &raquo;</a></p>
          </div><!-- /.col-lg-4 -->

          <div class="col-lg-3">
            <img class="img-circle" src="<?php echo base_url('assets/img/1.png')?>" alt="Generic placeholder image" width="140" height="140">
            <h2>Rekap Mahasiswa</h2>
            <p>Perekapan data setiap dosen yang mencakupi bimbingan, penguji, dan surat-surat lainnya.</p>
            <p><a class="btn btn-default" href="<?php echo site_url('Mahasiswa/rekap_mahasiswa')?>"</a>Lihat &raquo;</a></p>
          </div><!-- /.col-lg-4 -->

          <div class="col-lg-3">
            <img class="img-circle" src="<?php echo base_url('assets/img/1.png')?>" alt="Generic placeholder image" width="140" height="140">
            <h2>Masa Studi</h2>
            <p>Perekapan data masa studi mahasiswa selama di Universitas Bengkulu.</p>
            <p><a class="btn btn-default" href="<?php echo site_url('Mahasiswa/masa_studi')?>"</a>Lihat &raquo;</a></p>
          </div><!-- /.col-lg-4 -->

          <div class="col-lg-3">
            <img class="img-circle" src="<?php echo base_url('assets/img/1.png')?>" alt="Generic placeholder image" width="140" height="140">
            <h2>Lama Skripsi</h2>
            <p>Perekapan data lama skripsi mahasiswa teknik informatika.</p>
            <p><a class="btn btn-default" href="<?php echo site_url('Mahasiswa/lama_skripsi')?>"</a>Lihat &raquo;</a></p>
          </div><!-- /.col-lg-4 -->

        </div><!-- /.row -->
      <?=anchor('welcome/index', 'Kembali', 
      ['class'=>'btn btn-danger']) ?>
  </div>