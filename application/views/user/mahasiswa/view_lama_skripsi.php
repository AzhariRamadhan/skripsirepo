<div class="panel-heading"><h1><span class="label label-default">Data Mahasiswa</span></h1></div>
	<div class="panel-body">
			<table id="myTable" class="table table-striped table-bordered table-hover" style="background-color:#fff">
				<thead>
					<tr>
						<th data-field="No" data-sortable="true" >No</th>
                  <th data-field="NPM" data-sortable="true">NPM</th>
                  <th data-field="nama_mahasiswa" data-sortable="true">Nama Mahasiswa</th>
                  <th data-field="masa_studi"  data-sortable="true">Masa Skripsi</th>
					</tr>
				</thead>
				<tbody>
						<?php $no = 1; foreach($lama_skripsi as $r) :
						$data['r']=$r ?>
						<tr>
							<td><?php echo $no++ ?></td>
		                  <td><?php echo $r['NPM'] ?></td>
		                  <td><?php echo $r['nama_mahasiswa'] ?></td>
		                  <td><?php if($r['sidang']!=0){$a = datediff($r['seminar_proposal'], $r['sidang']); echo $a['years'].' tahun '.$a['months'].' bulan ';}else { echo 'Belum Selesai';} ?></td>      							
						</tr>
						<?php endforeach; ?>
				</tbody>
			</table>
	</div>
	<?php 
  function datediff($tgl1, $tgl2){
$tgl1 = strtotime($tgl1);
$tgl2 = strtotime($tgl2);
$diff_secs = abs($tgl1 - $tgl2);
$base_year = min(date("Y", $tgl1), date("Y", $tgl2));
$diff = mktime(0, 0, $diff_secs, 1, 1, $base_year);
return array( "years" => date("Y", $diff) - $base_year, "months_total" => (date("Y", $diff) - $base_year) * 12 + date("n", $diff) - 1, "months" => date("n", $diff) - 1, "days_total" => floor($diff_secs / (3600 * 24)), "days" => date("j", $diff) - 1, "hours_total" => floor($diff_secs / 3600), "hours" => date("G", $diff), "minutes_total" => floor($diff_secs / 60), "minutes" => (int) date("i", $diff), "seconds_total" => $diff_secs, "seconds" => (int) date("s", $diff) );
}
?>