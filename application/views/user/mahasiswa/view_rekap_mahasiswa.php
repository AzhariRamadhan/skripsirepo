<div class="panel-heading"><h1><span class="label label-default">Rekap Mahasiswa</span></h1></div>
	<div class="panel-body">
			<table id="myTable" class="table table-striped table-bordered table-hover" style="background-color:#fff">
				<thead>
					<tr>
						<th data-field="No" data-sortable="true" >No</th>
                  <th data-field="NPM" data-sortable="true">NPM</th>
                  <th data-field="nama_mahasiswa" data-sortable="true">Nama Mahasiswa</th>
                  <th data-field="judul"  data-sortable="true">Judul</th>
                  <th data-field="pembimbing1" data-sortable="true">Pembimbing 1</th>
                  <th data-field="pembimbing2" data-sortable="true">Pembimbing 2</th>
                  <th data-field="penguji1" data-sortable="true">Penguji 1</th>
                  <th data-field="penguji2" data-sortable="true">Penguji 2</th>
					</tr>
				</thead>
				<tbody>
						<?php $no = 1; foreach($rekap_mahasiswa as $r) :
						$data['r']=$r ?>
						<tr>
							<td><?php echo $no++ ?></td>
							<td><?php echo $r['NPM'] ?></td>
			                <td><?php echo $r['nama_mahasiswa'] ?></td>                  
			                  <td><?php echo $r['judul'] ?></td>
			                  <td><?php echo $r['pembimbing1'] ?></td>
			                  <td><?php echo $r['pembimbing2'] ?></td>
			                  <td><?php echo $r['penguji1'] ?></td>
			                  <td><?php echo $r['penguji2'] ?></td>							
						</tr>
						<?php endforeach; ?>
				</tbody>
			</table>
	</div>