<div class="panel-heading"><h1><span class="label label-default">Data Dosen</span></h1></div>
	<div class="panel-body">
			<table id="myTable" class="table table-striped table-bordered table-hover" style="background-color:#fff">
				<thead>
					<tr>
						<th>ID</th>
						<th>Nama</th>
						<th>Fakultas</th>
					</tr>
				</thead>
				<tbody>
						<?php foreach($data_dosen as $row) :
						$data['row']=$row ?>
						<tr>
							<td><?=$row->NIP?></td>
							<td><?=$row->Nama_Dosen?></td>
							<td><?=$row->Fakultas?></td>							
						</tr>
						<?php endforeach; ?>
				</tbody>
			</table>
	</div>