<div class="panel-heading"><h1><span class="label label-default">Data Mahasiswa</span></h1></div>
  <div class="panel-body">
      <table id="myTable" class="table table-striped table-bordered table-hover" style="background-color:#fff">
        <thead>
          <tr>
            <th data-field="No" data-sortable="true" >No</th>
                  <th data-field="NPM" data-sortable="true">NPM</th>
                  <th data-field="nama_mahasiswa" data-sortable="true">Nama Mahasiswa</th>
                  <th data-field="seminar_proposal"  data-sortable="true">Seminar Proposal</th>
                  <th data-field="seminar_hasil"  data-sortable="true">Seminar Hasil</th>
                  <th data-field="sidang"  data-sortable="true">Sidang</th>
                  <th data-field="IPK"  data-sortable="true">IPK</th>
          </tr>
        </thead>
        <tbody>
            <?php $no=1; foreach($jadwal as $r) :
            $data['r']=$r ?>
            <tr>
                  <td><?php echo $no++ ?></td>
                  <td><?php echo $r['NPM'] ?></td>
                  <td><?php echo $r['nama_mahasiswa'] ?></td>                  
                  <td><?php echo $r['seminar_proposal'] ?></td>
                  <td><?php echo $r['seminar_hasil'] ?></td>
                  <td><?php echo $r['sidang'] ?></td>
                  <td><?php echo $r['IPK'] ?></td>            
            </tr>
            <?php endforeach; ?>
        </tbody>
      </table>
  </div>