<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_seminar_proposal extends CI_Model {
	public function all(){
		$hasil = $this->db->query("SELECT * FROM tb_mahasiswa, tb_seminarproposal WHERE tb_mahasiswa.NPM = tb_seminarproposal.npm ");
		if($hasil->num_rows() > 0){
			return $hasil->result();
		}else{
			return array();
		}
	}

	
	public function create($data_anggota){
		$this->db->insert('tb_seminarproposal', $data_anggota);
	}

	public function update($npm, $data_anggota){
		$this->db->where('npm', $npm)
				 ->update('tb_seminarproposal', $data_anggota);
	}

	public function delete($npm){
		$this->db->where('npm', $npm)
				 ->delete('tb_seminarproposal');
	}
}	