<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_mahasiswa extends CI_Model {
	public function all(){
		$hasil = $this->db->get('tb_mahasiswa');
		if($hasil->num_rows() > 0){
			return $hasil->result();
		}else{
			return array();
		}
	}

	public function create($data_anggota){
		$this->db->insert('tb_mahasiswa', $data_anggota);
	}

	public function update($npm, $data_anggota){
		$this->db->where('id_mahasiswa', $npm)
				 ->update('tb_mahasiswa', $data_anggota);
	}

	public function update_toefl($npm, $data_anggota){
		$this->db->where('NPM', $npm)
				 ->update('tb_mahasiswa', $data_anggota);
	}

	public function delete($id){
		$this->db->where('id_mahasiswa', $id)
				 ->delete('tb_mahasiswa');
	}

	public function getMahasiswa($id) {
		return $this->db->where("id_mahasiswa", $id)->get("tb_mahasiswa")->result();
	}

	public function cariMahasiswa($npm, $id){
		return $this->db->where("NPM", $npm)->where('id_mahasiswa !=', $id)->get("tb_mahasiswa")->result();
	}



	public function cariMahasiswaDenganNPM($npm){
		return $this->db->where("NPM", $npm)->get("tb_mahasiswa")->result();
	}


	public function masa_studi(){
		
	$query = $this->db->query("SELECT tb_mahasiswa.*, tb_seminarproposal.sidang FROM tb_mahasiswa 
          LEFT JOIN tb_seminarproposal ON tb_mahasiswa.NPM = tb_seminarproposal.npm");
        return $query->result_array();
	}

	public function detail_mahasiswa(){
		
	$query = $this->db->query("SELECT tb_mahasiswa.*, tb_seminarproposal.* FROM tb_mahasiswa 
          LEFT JOIN tb_seminarproposal ON tb_mahasiswa.NPM = tb_seminarproposal.npm");
        return $query->result();
	}

	public function rekap_mahasiswa(){
        $query = $this->db->query("SELECT tb_mahasiswa.NPM, tb_mahasiswa.nama_mahasiswa, tb_sempro.judul, 
          (select Nama_Dosen from tb_dosen where tb_dosen.NIP=tb_sempro.pembimbing1) as pembimbing1, 
          (select Nama_Dosen from tb_dosen where tb_dosen.NIP=tb_sempro.pembimbing2) as pembimbing2, 
          (select Nama_Dosen from tb_dosen where tb_dosen.NIP=tb_sempro.penguji1) as penguji1, 
          (select Nama_Dosen from tb_dosen where tb_dosen.NIP=tb_sempro.penguji2) as penguji2 FROM  
          tb_mahasiswa JOIN tb_sempro on tb_mahasiswa.NPM = tb_sempro.NPM ");
        return $query->result_array();
    }

    function lama_skripsi(){
        $query = $this->db->query("SELECT * from tb_seminarproposal JOIN tb_mahasiswa ON 
          tb_mahasiswa.NPM = tb_seminarproposal.npm ");
        return $query->result_array();
    }
}
