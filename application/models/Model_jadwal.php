<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_jadwal extends CI_Model {
	public function all(){
		$hasil = $this->db->get('peminjaman');
		if($hasil->num_rows() > 0){
			return $hasil->result();
		}else{
			return array();
		}
	}

	public function create($data_anggota){
		$this->db->insert('jadwal', $data_anggota);
	}

	public function update($npm, $data_anggota){
		$this->db->where('id_jadwal', $npm)
				 ->update('jadwal', $data_anggota);
	}

	public function delete($npm){
		$this->db->where('id_jadwal', $npm)
				 ->delete('jadwal');
	}
}	