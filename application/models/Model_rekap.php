<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Model_rekap extends CI_Model
{
	public function rekap_akhir(){
		return $this->db->query("SELECT tb_mahasiswa.NPM, nama_mahasiswa,TIMESTAMPDIFF(MONTH, seminar_proposal, sidang) AS 
lama_skripsi, TIMESTAMPDIFF(MONTH,tanggal_masuk,sidang) AS lama_studi FROM tb_mahasiswa INNER JOIN 
tb_seminarproposal ON tb_mahasiswa.NPM = tb_seminarproposal.npm WHERE sidang IS NOT NULL")->result();
	}
}
