<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_judul extends CI_Model {
	public function all(){
		$hasil = $this->db->get('tb_sempro');
		if($hasil->num_rows() > 0){
			return $hasil->result();
		}else{
			return array();
		}
	}


	public function create($data_anggota){
		$this->db->insert('tb_sempro', $data_anggota);
	}

	public function update($npm, $data_anggota){
		$this->db->where('NPM', $npm)
				 ->update('tb_sempro', $data_anggota);
	}

	public function delete($npm){
		$this->db->where('NPM', $npm)
				 ->delete('tb_sempro');
	}

	public function masa_studi(){
		
	$query = $this->db->query("SELECT tb_sempro.*, tb_seminarproposal.sidang FROM tb_sempro 
          LEFT JOIN tb_seminarproposal ON tb_sempro.NPM = tb_seminarproposal.npm");
        return $query->result_array();
	}

	function rekap_judul(){
        $query = $this->db->query("SELECT * FROM tb_mahasiswa, tb_sempro WHERE tb_mahasiswa.NPM = tb_sempro.npm ");
        return $query->result();
    }

    function rekap_judulskripsi(){
        $query = $this->db->query("SELECT * FROM tb_mahasiswa, tb_seminarproposal WHERE tb_mahasiswa.NPM = tb_seminarproposal.npm ");
        return $query->result_array();
    }

    public function tgl_proposal(){
	$this->db->distinct();
	$this->db->select('seminar_proposal');
	$cari=$this->db->get('tb_seminarproposal');
	return $cari->result(); 
	}		

    function cetak_proposal($tgl){
    	
        $query = $this->db->query("SELECT * FROM tb_mahasiswa, tb_seminarproposal WHERE tb_mahasiswa.NPM = tb_seminarproposal.npm and tb_seminarproposal.seminar_proposal = '$tgl'");
        return $query->result();
    }

    public function tgl_hasil(){
	$this->db->distinct();
	$this->db->select('seminar_hasil');
	$cari=$this->db->get('tb_seminarproposal');
	return $cari->result(); 
	}		

    function cetak_hasil($tgl){
    	
        $query = $this->db->query("SELECT * FROM tb_mahasiswa, tb_seminarproposal WHERE tb_mahasiswa.NPM = tb_seminarproposal.npm and tb_seminarproposal.NPM = '$tgl'");
        return $query->result();
    }

    public function tgl_sidang(){
	$this->db->distinct();
	$this->db->select('sidang');
	$cari=$this->db->get('tb_seminarproposal');
	return $cari->result(); 
	}		

    function cetak_sidang($tgl){
    	
        $query = $this->db->query("SELECT * FROM tb_mahasiswa, tb_seminarproposal WHERE tb_mahasiswa.NPM = tb_seminarproposal.npm and tb_seminarproposal.NPM = '$tgl'");
        return $query->result();
    }

    public function cariJudulDenganNPM($npm){
		return $this->db->where("NPM", $npm)->get("tb_sempro")->result();
	}

	public function cariJudulSempropDenganNPM($npm){
		return $this->db->where("NPM", $npm)->where('judul IS NOT NULL', null, false)
			->get("tb_seminarproposal")->result();
	}

	public function cariJudulSemhasDenganNPM($npm){
		return $this->db->where("NPM", $npm)->where('judul_hasil IS NOT NULL', null, false)
			->get("tb_seminarproposal")->result();
	}


}	
