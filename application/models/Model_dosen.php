<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_dosen extends CI_Model {

	public function detailDosen($id){
		return $this->db->where("id_dosen", $id)->get("tb_dosen")->result();
	}

	public function cariDosen($nip, $id){
		return $this->db->where("NIP", $nip)->where('id_dosen !=', $id)->get("tb_dosen")->result();
	}


	public function all(){
		$this->db->where('Prodi','Informatika');
        $query = $this->db->get('tb_dosen');
        return $query->result();
	}

	public function dataDosen(){
		return $this->db->query("SELECT *, (SELECT COUNT(*) FROM tb_sempro WHERE pembimbing1 = NIP OR pembimbing2 = NIP) 
AS jumlah_dibimbing, (SELECT COUNT(*) FROM tb_sempro WHERE penguji1 = NIP OR penguji2 = NIP) AS jumlah_diuji FROM tb_dosen")
			->result();
	}

	public function dataDibimbing($nip) {
		return $this->db->query("SELECT * FROM tb_sempro WHERE pembimbing1 = " . $nip . " OR pembimbing2 = ".$nip)->result();
	}

	public function dataDiuji($nip) {
		return $this->db->query("SELECT * FROM tb_sempro WHERE penguji1 = " . $nip . " OR penguji2 = ".$nip)->result();
	}

	public function nama_dosen($nip){
        $query = $this->db->query("SELECT nama_dosen from tb_dosen
		 where NIP = '$nip'
		");
        return $query->result();
	}

	public function rekap_bimbing_uji($nip,$status)
  	{
     $data = array(
      'isi' => 'tabel_daftarbimbingan',
      'status' => $status,
      'dosen' => $this->model_admin->get_nama_dosen($nip),
      'data'  => $this->model_admin->rekap_bimbingan($nip,$status)
     );
    $this->load->view('layout',$data);
  	}


  	function get_nama_dosen($nip){
        $this->db->select('NIP,Nama_Dosen');
        $this->db->where('NIP',$nip);
         $query = $this->db->get('tb_dosen');
        return $query->result();
    }

	function pp(){
        $query = $this->db->query("SELECT tb_mahasiswa.NPM, tb_mahasiswa.nama_mahasiswa, tb_sempro.judul, 
          (select Nama_Dosen from tb_dosen where tb_dosen.NIP=tb_sempro.pembimbing1) as pembimbing1, 
          (select Nama_Dosen from tb_dosen where tb_dosen.NIP=tb_sempro.pembimbing2) as pembimbing2, 
          (select Nama_Dosen from tb_dosen where tb_dosen.NIP=tb_sempro.penguji1) as penguji1, 
          (select Nama_Dosen from tb_dosen where tb_dosen.NIP=tb_sempro.penguji2) as penguji2 FROM  
          tb_mahasiswa JOIN tb_sempro on tb_mahasiswa.NPM = tb_sempro.NPM ");
        return $query->result();
    }

     function cetak_hasil($tgl){
    $query = $this->db->query("SELECT 
      (select Nama_Dosen from tb_dosen where tb_dosen.NIP=tb_sempro.pembimbing1) as pembimbing1, 
      (select Nama_Dosen from tb_dosen where tb_dosen.NIP=tb_sempro.pembimbing2) as pembimbing2, 
      (select Nama_Dosen from tb_dosen where tb_dosen.NIP=tb_sempro.penguji1) as penguji1, 
      (select Nama_Dosen from tb_dosen where tb_dosen.NIP=tb_sempro.penguji2) as penguji2
      FROM tb_sempro
      inner join tb_seminarproposal ON tb_sempro.NPM=tb_seminarproposal.npm
      where tb_seminarproposal.npm = '$tgl' " );
    return $query->result();
  }
    
	
	public function create($data_anggota){
		$this->db->insert('tb_dosen', $data_anggota);
	}

	public function update($nip, $data_anggota){
		$this->db->where('id_dosen', $nip)
				 ->update('tb_dosen', $data_anggota);
	}

	public function delete($id){
		$this->db->where('id_dosen', $id)
				 ->delete('tb_dosen');
	}

	// update dosen pembimbing_dan penguji
	public function create_pp($nip, $data_anggota){
		$this->db->where('NPM', $nip)
					->update('tb_sempro', $data_anggota);
	}



}
