<?php


class Model_user extends CI_Model
{
    public function create_dosen($data) {
        $this->db->insert("user", $data);
    }

    public function create_mahasiswa($data) {
        $this->db->insert("user", $data);
    }

    public function update($id, $data) {
    	$this->db->where("username", $id)->update("user", $data);
	}

	public function delete($id){
    	$this->db->where("username", $id)->delete("user");
	}
}
